<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('additional_bill_holder')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('terms_and_conditions');
            $table->string('phone_number');
            $table->string('social_security');
            $table->date('dob');
            $table->string('address');
            $table->string('street_name');
            $table->string('street_number');
            $table->string('city');
            $table->string('zip_code');
            $table->string('country');
            $table->string('hear_about_us');
            $table->string('promo_code');
            $table->enum('role', ['admin', 'user']);
            $table->boolean('deleted');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
