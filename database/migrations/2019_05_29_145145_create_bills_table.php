<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('bill_no')->unique();
            $table->bigInteger('user_id');
            $table->string('user');
            $table->string('bill_type');
            $table->string('service_provider');
            $table->string('duration_of_service');
            $table->string('image_url');
            $table->date('date');
            $table->boolean('renew_service');
            $table->string('pass_code')->nullable();
            $table->longText('comment')->nullable();
            $table->string('status');
            $table->boolean('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
