<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_no')->unique();
            $table->string('bill_id');
            $table->bigInteger('user_id');
            $table->string('user');
            $table->string('bill_type');
            $table->string('service_provider');
            $table->date('bill_date');
            $table->timestamp('bill_created_date');
            $table->decimal('original_amount', 8, 2);
            $table->decimal('new_amount', 8, 2);
            $table->decimal('negotiated_amount', 8, 2);
            $table->decimal('our_charge', 8, 2);
            $table->decimal('paying_amount', 8, 2);
            $table->string('paypal_id');
            $table->string('status');
            $table->boolean('deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
