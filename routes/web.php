<?php

Route::get('/', 'HomeController@index'); 

Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');



// Front site routes
Route::get('/about-us', 'AboutController@index');
Route::get('/how-it-works', 'HowItWorksController@index');
// Route::get('/faq', 'FAQController@userIndex');
Route::get('/faq', 'UserFaqController@index');



// Admin dashboard routes
// Route::get('/admin/dashboard', array('before' => 'auth', 'uses' => 'AdminController@index'));


Route::get('/admin/login', 'SessionsAdminController@create');
Route::post('/admin/login', 'SessionsAdminController@store');
Route::get('/admin/logout', 'SessionsAdminController@destroy');


Route::get('/admin/dashboard', 'AdminController@index');

Route::get('/admin/dashboard/bill-types', 'BillTypeController@index');
Route::post('/admin/dashboard/bill-types', 'BillTypeController@store');
Route::post('/admin/dashboard/bill-types/update/{billId}', 'BillTypeController@update');
Route::get('/admin/dashboard/bill-types/delete/{billType}', 'BillTypeController@destroy');

Route::get('/admin/dashboard/service-providers', 'ServiceProviderController@index');
Route::post('/admin/dashboard/service-providers', 'ServiceProviderController@store');
Route::post('/admin/dashboard/service-providers/update/{serviceProvider}', 'ServiceProviderController@update');
Route::get('/admin/dashboard/service-providers/delete/{serviceProvider}', 'ServiceProviderController@destroy');

Route::get('/admin/dashboard/country', 'CountryController@index');
Route::post('/admin/dashboard/country', 'CountryController@store');
Route::post('/admin/dashboard/country/update/{country}', 'CountryController@update');

Route::get('/admin/dashboard/hear-about-us', 'HearAboutUsController@index');
Route::post('/admin/dashboard/hear-about-us', 'HearAboutUsController@store');
Route::post('/admin/dashboard/hear-about-us/update/{hearAboutUs}', 'HearAboutUsController@update');
Route::get('/admin/dashboard/hear-about-us/delete/{hearAbout}', 'HearAboutUsController@destroy');

Route::get('/admin/dashboard/faq', 'FAQController@index');
Route::get('/admin/dashboard/faq/create', 'FAQController@create');
Route::get('/admin/dashboard/faq/update/{faq}', 'FAQController@edit');
Route::post('/admin/dashboard/faq/update/{faq}', 'FAQController@update');
Route::get('/admin/dashboard/faq/show/{faq}', 'FAQController@show');
Route::post('/admin/dashboard/faq', 'FAQController@store');
Route::get('/admin/dashboard/faq/delete/{faq}', 'FAQController@destroy');

Route::get('/admin/dashboard/user-testimonials', 'UserTestimonialController@index');
Route::get('/admin/dashboard/user-testimonials/create', 'UserTestimonialController@create');
Route::post('/admin/dashboard/user-testimonials/update/{userTestimonial}', 'UserTestimonialController@update');
Route::get('/admin/dashboard/user-testimonials/update/{userTestimonial}', 'UserTestimonialController@edit');
Route::get('/admin/dashboard/user-testimonials/show/{userTestimonial}', 'UserTestimonialController@show');
Route::post('/admin/dashboard/user-testimonials', 'UserTestimonialController@store');
Route::get('/admin/dashboard/user-testimonials/delete/{userTestimonial}', 'UserTestimonialController@destroy');

Route::get('/admin/dashboard/manage-users', 'UserController@index');
Route::get('/admin/dashboard/manage-users/show/{user}', 'UserController@show');
Route::get('/admin/dashboard/manage-users/delete/{user}', 'UserController@destroy');

Route::get('/admin/dashboard/manage-invoices', 'InvoiceController@index');
Route::post('/admin/dashboard/manage-invoices', 'InvoiceController@store');
Route::get('/admin/dashboard/manage-invoices/show/{invoice}', 'InvoiceController@show');
Route::get('/admin/dashboard/manage-invoices/create/{bill}&{user}', 'InvoiceController@create');
Route::post('/admin/dashboard/manage-invoices/update/{invoice}', 'InvoiceController@update');
Route::get('/admin/dashboard/manage-invoices/update/{invoice}', 'InvoiceController@edit');
Route::get('/admin/dashboard/manage-invoices/delete/{invoice}', 'InvoiceController@destroy');

Route::get('/admin/dashboard/bills', 'BillController@index');
Route::post('/admin/dashboard/bills', 'BillController@edit');
Route::get('/admin/dashboard/bills/show/{bill}', 'BillController@show');
Route::get('/admin/dashboard/bills/delete/{bill}', 'BillController@destroy');

Route::get('/admin/dashboard/payments', 'PaymentsController@index');
Route::get('/admin/dashboard/payments/show/{payment}', 'PaymentsController@show');
Route::get('/admin/dashboard/payments/delete/{payment}', 'PaymentsController@destroy');






// User dashboard routes
Route::post('/dashboard', 'DashboardController@index');
Route::get('/dashboard', 'DashboardController@index')->name('user-dashboard');

Route::post('/dashboard/bill-payment/paypal', 'PaymentController@payWithpaypal');
Route::get('/dashboard/bill-payment/status', 'PaymentController@getPaymentStatus');

Route::get('/dashboard/add-my-bill', 'AddBillController@create');
Route::post('/dashboard/add-my-bill', 'AddBillController@store');

Route::get('/dashboard/invoices', 'DashboardController@invoices');

Route::get('/dashboard/edit-my-account', 'DashboardController@editAccount');
Route::post('/dashboard/edit-my-account', 'DashboardController@updateAccount');

Route::get('/dashboard/account-settings', 'DashboardController@editAccountSettings');
Route::post('/dashboard/account-settings', 'DashboardController@updateAccountSettings');
