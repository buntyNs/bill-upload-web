<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    protected $fillable = [
        'bill_no',
        'bill_type',
        'user_id',
        'user',
        'service_provider',
        'duration_of_service',
        'image_url',
        'date',
        'renew_service',
        'pass_code',
        'comment',
        'status',
        'deleted'
    ];

    protected $attributes = [
        'bill_no' => 'B1',
        'renew_service' => 1,
        'status' => 'uploaded',
        'deleted' => 0
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public static function billNotification()
    {
        return count(Bill::all()->where('deleted', '=', 0)->where('status', '=', 'uploaded'));
    }

    public static function billNotifications()
    {
        return $bills = Bill::all()->where('deleted', '=', 0)->where('status', '=', 'uploaded');
    }
}
