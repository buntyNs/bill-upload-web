<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //NEW: Import Schema

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191); //NEW: Increase StringLength

        view()->composer('layouts-admin.navbar', function($view) {
            $view->with('billNotification', \App\Bill::billNotification());
        });

        view()->composer('layouts-admin.navbar', function($view) {
            $view->with('billNotifications', \App\Bill::billNotifications());
        });
    }
}
