<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable = [
        'bill_id',
        'invoice_no',
        'bill_type',
        'service_provider',
        'user',
        'user_id',
        'bill_date',
        'bill_created_date',
        'original_amount',
        'new_amount',
        'negotiated_amount',
        'our_charge',
        'paying_amount',
        'paypal_id',
        'status',
        'deleted'
    ];

    protected $attributes = [
        'invoice_no' => 'IN1',
        'status' => 'published',
        'deleted' => 0
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bill()
    {
        return $this->belongsTo(Bill::class);
    }
}
