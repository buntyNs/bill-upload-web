<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\Invoice;
use App\Country;
use App\User;
use Auth;
use DB;
use Hash;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:user');
    }

    public function paypal()
    {
        return view('dashboard.welcome');
    }

    public function index()
    {
        $userId = Auth::user()->id;
        $uploadedBills = Bill::all()->where('deleted', '=', 0)
                            ->where('user_id', '=', $userId)
                            ->where('status', '=', 'uploaded');
        $invoicedBills = Bill::join('invoices','invoices.bill_id','bills.bill_no')
                                ->where('bills.user_id', '=', $userId)
                                ->where('bills.status', '=', 'invoiced')
                                ->where('bills.deleted', '=', 0)
                                ->select("bills.bill_no", "bills.bill_type", "bills.created_at", "bills.service_provider", "invoices.paying_amount", "bills.status")
                                ->get();

        // $billdInvoice = Invoice::all()->where('bill_id', '=', );
        return view('dashboard.index', compact('uploadedBills', 'invoicedBills'));
    }

    public function addBill()
    {
        return view('dashboard.add-my-bill');
    }

    public function invoices()
    {
        $user_id = Auth::user()->id;
        // dd($user_id);
        $paidInvoices = Invoice::all()->where('status', '=', 'paid')->where('user_id', '=', $user_id);
        return view('dashboard.invoices', compact('paidInvoices'));
    }

    public function editAccount()
    {
        $user_id = Auth::user()->id;
        $countries = Country::all();
        $accountDetails = User::find($user_id);
        return view('dashboard.edit-my-account', compact('accountDetails', 'countries'));
    }

    public function updateAccount(Request $request)
    { 
        $request->validate([
            'first_name' => 'bail|required|max:50|min:2',
            'last_name'  => 'bail|required|max:50|min:2',
            'additional_bill_holder' => 'nullable|max:50|min:2',
            'phone_number' => 'required|regex:/(0)[0-9]{9}/',
            'street_name' => 'required|max:50|min:2',
            'street_number' => 'required|numeric',
            'city' => 'required|max:30|min:2',
            'zip_code' => 'required|numeric',
            'country' => 'required|max:30|min:2'
        ]);
            
        $user_id = Auth::user()->id;
        $streetName = request('street_name');
        $streetNumber = request('street_number');
        $city = request('city');
        $zipCode = request('zip_code');
        $country = request('country');
        $address = $streetName . ", " . $streetNumber . ", " . $city . ", " . $zipCode . ", " . $country;
        
        DB::beginTransaction();
        try {
            User::whereId($user_id)->update([
                'first_name' => request('first_name'), 
                'last_name' => request('last_name'), 
                'additional_bill_holder' => request('additional_bill_holder'),
                'phone_number' => request('phone_number'),
                'street_name' => request('street_name'),
                'street_number' => request('street_number'),
                'city' => request('city'),
                'zip_code' => request('zip_code'),
                'country' => request('country'),
                'address' => $address
            ]);

            DB::commit();
            session()->flash('message', 'Successfully updated your profile');
            return redirect('/dashboard');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/dashboard');
        }
    }

    public function editAccountSettings()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        return view('dashboard.account-settings', compact('user'));
    }

    public function updateAccountSettings(Request $request)
    {
        $request->validate([
            'email' => 'bail|required|email',
            'password' => 'bail|required|confirmed'
        ]);

        $user_id = Auth::user()->id;
        $hashedPassword = Hash::make(request('password'));
        
        DB::beginTransaction();
        try {
            User::whereId($user_id)->update([
                'email' => request('email'),
                'password' => $hashedPassword
            ]);
            DB::commit();
            auth()->logout();
            session()->flash('message', 'Successfully updated the account settings, please login with new credentials');
            return redirect('/login');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/dashboard');
        }
    }
}
