<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(User $users)
    {
        $users = $users->all()->where('deleted', '=', 0);
        return view('admin.manage-users.index', compact('users'));
    }

    public function show(User $user)
    {
        $user_id = $user->id;
        $user = User::find($user_id);
        return view('admin.manage-users.show', compact('user'));
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('users')->where('id', '=', $id)->update(['deleted' => 1]);
            $deletedUser = DB::table('users')->select('first_name')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the user ' . $deletedUser);
            return redirect('/admin/dashboard/manage-users');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/manage-users');
        }
    }
}
