<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SessionsAdminController extends Controller
{
    public function __construct()
	{
		$this->middleware('guest')->except(['destroy']);
    }
    
    public function create()
    {
        return view('admin.sessions.create');
    }

    public function store(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed
            return redirect('/admin/dashboard');
        }
    
        return back()->withErrors([
            'message' => 'Please check your credentials and try again'
        ]);
    }

    public function destroy()
    {
    	auth()->logout();
    	return redirect('/admin/login');
    }
}
