<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HearAboutUs;
use DB;

class HearAboutUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(HearAboutUs $hearAboutUs)
    {
        $hearAboutUs = $hearAboutUs->all()->where('deleted', '=', 0);
        return view('admin.hear-about-us.index', compact('hearAboutUs'));
    }

    public function create()
    {
        return view('admin.hear-about-us.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'hear-about-us' => 'bail|required|max:20|min:3'
        ]);
        DB::beginTransaction();
        try {
            $hearAboutUs = HearAboutUs::create([
                'name' => request('hear-about-us'),
                'deleted' => 0
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added a new hear about us');
            return redirect('/admin/dashboard/hear-about-us');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/hear-about-us');
        }
    }

    // public function update($id)
    // {
    //     DB::beginTransaction();
    //     try {
    //         DB::table('hear_about_us')->where('id', '=', $id)->update(['name' => request('hear-about-us')]);
    //         DB::commit();
    //         session()->flash('message', 'Successfully updated the hear about us');
    //         return redirect('/admin/dashboard/hear-about-us');
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         session()->flash('message-error', 'Something went wrong, please try again');
    //         return redirect('/admin/dashboard/hear-about-us');
    //     }
    // }

    public function update(Request $request, $id)
    {
        $request->validate([
            'hear-about-us' => 'bail|required|max:20|min:3',
        ]);
        DB::beginTransaction();
        try {
            DB::table('hear_about_us')->where('id', '=', $id)->update(['name' => request('hear-about-us')]);
            DB::commit();
            session()->flash('message', 'Successfully updated the hear about us');
            return redirect('/admin/dashboard/hear-about-us');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/hear-about-us');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('hear_about_us')->where('id', '=', $id)->update(['deleted' => 1]);
            $hearAboutUsName = DB::table('hear_about_us')->select('name')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the hear about us ' . $hearAboutUsName);
            return redirect('/admin/dashboard/hear-about-us');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/hear-about-us');
        }
    }
}
