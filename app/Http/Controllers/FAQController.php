<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use DB;

class FAQController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function userIndex(Faq $faqs)
    {
        $faqs = $faqs->all()->where('deleted', '=', 0)->where('status', '=', 'published');
        return view('faq', compact('faqs'));
    }

    public function index(Faq $faqs)
    {
        $faqs = $faqs->all()->where('deleted', '=', 0);
        return view('admin.faq.index', compact('faqs'));
    }

    public function create()
    {
        return view('admin.faq.create');
    }

    public function show(Faq $faq)
    {
        $faq_id = $faq->id;
        $faq = Faq::find($faq_id);
        return view('admin.faq.show', compact('faq'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'topic' => 'required|max:50|min:3',
            'body' => 'required|max:500|min:10',
            'status' => 'required|in:published,unpublished'
        ]);
        DB::beginTransaction();
        try {
            $faq = FAQ::create([
                'topic' => request('topic'),
                'body' => request('body'),
                'status' => request('status'),
                'deleted' => 0
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added a new FAQ');
            return redirect('/admin/dashboard/faq');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/faq');
        }
    }

    public function edit($id)
    {
        $faqId = $id;
        $faq = Faq::find($faqId);
        return view('admin.faq.update', compact('faq'));
    }

    // public function update($id)
    // {
    //     $topic = request('topic');
    //     $body = request('body');
    //     $status = request('status');
    //     DB::beginTransaction();
    //     try {
    //         DB::table('faqs')->where('id', '=', $id)->update([
    //             'topic' => $topic,
    //             'body' => $body,
    //             'status' => $status
    //         ]);
    //         DB::commit();
    //         session()->flash('message', 'Successfully updated the FAQ');
    //         return redirect('/admin/dashboard/faq');
    //     } catch (\Exception $e) {
    //         DB::rollback();
    //         session()->flash('message-error', 'Something went wrong, please try again');
    //         return redirect('/admin/dashboard/faq');
    //     }   
    // }

    public function update(Request $request, $id)
    {
        $request->validate([
            'topic' => 'required|max:50|min:3',
            'body' => 'required|max:500|min:10',
            'status' => 'required|in:published,unpublished'
        ]);

        DB::beginTransaction();
        try {
            DB::table('faqs')->where('id', '=', $id)->update([
                'topic' => request('topic'),
                'body' => request('body'),
                'status' => request('status')
            ]);
            DB::commit();
            session()->flash('message', 'Successfully updated the FAQ');
            return redirect('/admin/dashboard/faq');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/faq');
        }   
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('faqs')->where('id', '=', $id)->update(['deleted' => 1]);
            $faqTopic = DB::table('faqs')->select('topic')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the FAQ ' . $faqTopic);
            return redirect('/admin/dashboard/faq');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/faq');
        }
    }
}
