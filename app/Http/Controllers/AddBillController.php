<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Bill;
use App\User;
use App\BillType;
use App\ServiceProvider;
use DB;

class AddBillController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:user');
    }

    public function create()
    {
        $billTypes = BillType::all();
        $serviceProviders = ServiceProvider::all();
        return view('dashboard.add-my-bill', compact('billTypes', 'serviceProviders'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'type-of-bill' => 'bail|required',
            'service-provider'  => 'bail|required',
            'duration-of-service' => 'bail|required',
            'bill-upload' => 'bail|required',
            'bill-date' => 'bail|date',
            'renew-service' => 'bail|required',
            'passcode' => 'nullable|max:20|min:4',
            'bill-comment' => 'nullable|max:300|min:3'
        ]);
        
        $filePath = Storage::disk('public')->put("bills", request('bill-upload'));

        if (count(Bill::all()) > 0) {
            $lastBillId = Bill::all()->last()->id;
            $lastBillId = $lastBillId + '1';
            $billNo = 'B' . $lastBillId;
        } elseif (count(Bill::all()) == 0) {
            $billNo = 'B1';
        }

        DB::beginTransaction();
        try {
            $user = Bill::create([
                'bill_no' => $billNo,
                'user_id' => Auth::user()->id,
                'user' => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                'bill_type' => request('type-of-bill'),
                'service_provider' => request('service-provider'),
                'duration_of_service' => request('duration-of-service'),
                'image_url' => $filePath,
                'date' => request('bill-date'),
                'pass_code' => request('passcode'),
                'comment' => request('bill-comment')
            ]);
            DB::commit();
            session()->flash('message', 'Successfully uploaded the bill');
            return redirect('/dashboard');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/dashboard');
        }
    }

}
