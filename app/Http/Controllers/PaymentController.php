<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;

use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use Config;
use URL;
use Route;
use App\Bill;
use App\Invoice;
use App\User;
use App\Payments;
use Auth;
use DB;

class PaymentController extends Controller
{
    private $_api_context;

    public function __construct()
    {
        $this->middleware('role:user');

        /** PayPal api context **/
        $paypal_conf = Config::get('paypal');
        // dd($paypal_conf);

        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
       
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {   
        $userId = Auth::user()->id;
        $formAmount = $request->get('amount');
        $billDetails = Bill::join('invoices','invoices.bill_id','bills.bill_no')
                                ->where('bills.user_id', '=', $userId)
                                ->where('bills.status', '=', 'invoiced')
                                ->where('bills.deleted', '=', 0)
                                ->select("invoices.paying_amount", "bills.id")
                                ->get();

        foreach ($billDetails as $billDetail) {
            $amount = $billDetail->paying_amount;
            $id = $billDetail->id;
        }

        $dbAmount = $amount;
        // dd($rec);
        // dd($id);
        // dd($dbAmount);

        if ($formAmount !== $dbAmount) {
            $theAmount = $dbAmount;
        }
        $theAmount = $dbAmount;

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $item_1 = new Item();

        $item_1->setName('Item 1') /** item name **/
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($theAmount); /** unit price **/
        
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')
               ->setTotal($dbAmount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
                    ->setItemList($item_list)
                    ->setDescription('Your transaction description');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('/dashboard/bill-payment/status/'))        // transaction pass
                      ->setCancelUrl(URL::to('/dashboard/bill-payment/status/'));    // transaction fail

        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

        try {
            // Payment creation
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                session()->flash('message-error', 'Connection timeout');
                return Redirect::to('/dashboard');
            } else {
                session()->flash('message-error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/dashboard');
            }
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
        session()->flash('message-error', 'Unknown error occurred');
        return Redirect::to('/dashboard');
    }


    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            session()->flash('message-error', 'Payment failed');
            return Redirect::route('/dashboard');
        }
        
        $payment = Payment::get($payment_id, $this->_api_context);
      
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
    
        if ($result->getState() == 'approved') {
            $transactionId = $payment->getTransactions()[0]->getRelatedResources()[0]->getSale()->getId();
            
            
            $billDetails = Bill::join('invoices','invoices.bill_id','bills.bill_no')
                                        ->where('bills.user_id', '=', Auth::user()->id)
                                        ->where('bills.status', '=', 'invoiced')
                                        ->where('bills.deleted', '=', 0)
                                        ->select("invoices.paying_amount", "bills.id", "bills.bill_no", "invoices.invoice_no", "invoices.id" , "invoices.user_id")
                                        ->get();

            foreach ($billDetails as $billDetail) {
                $amount = $billDetail->paying_amount;
                $userId = $billDetail->user_id;
                $billNo = $billDetail->bill_no;
                $billId = $billDetail->id;
                $invoiceId = $billDetail->id;
                $invoiceNo = $billDetail->invoice_no;
            }
            
            DB::beginTransaction();
            try {
                // Insert to the payment table according to the approved status
                // DB::table('payments')->insert(
                //     ['bill_id' => $billNo, 'invoice_id' => $invoiceNo, 'user_id' => $userId, 'transaction_id' => $transactionId, 'payment_id' => $payment_id, 'amount' => $amount]
                // );
                $payment = Payments::create([
                    'bill_id' => $billNo,
                    'invoice_id' => $invoiceNo,
                    'user_id' => $userId,
                    'transaction_id' => $transactionId,
                    'payment_id' => $payment_id,
                    'amount' => $amount,
                ]);

                // dd($invoiceNo);
                // Update the bills, invoices table
                DB::table('bills')->where('id', '=', $billId)->update(['status' => 'paid']);
                DB::table('invoices')->where('id', '=', $invoiceId)->update(['status' => 'paid']);
                DB::commit();
                session()->flash('message', 'Payment success');
                return Redirect::to('/dashboard');
            } catch (\Exception $e) {
                DB::rollback();
                dd($e);
            }
        }
        session()->flash('message-error', 'Payment failed');
        return Redirect::to('/dashboard');
    }
}
