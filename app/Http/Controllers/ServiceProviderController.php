<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceProvider;
use DB;

class ServiceProviderController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(ServiceProvider $serviceProviders)
    {
        $serviceProviders = $serviceProviders->all()->where('deleted', '=', 0);
        return view('admin.service-provider.index', compact('serviceProviders'));
    }

    public function create()
    {
        return view('admin.service-provider.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'service-provider' => 'bail|required|max:20|min:3'
        ]);
        DB::beginTransaction();
        try {
            $serviceProvider = ServiceProvider::create([
                'name' => request('service-provider'),
                'deleted' => 0
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added a new service provider');
            return redirect('/admin/dashboard/service-providers');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/service-providers');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'service-provider' => 'bail|required|max:20|min:3'
        ]);
        DB::beginTransaction();
        try {
            DB::table('service_providers')->where('id', '=', $id)->update(['name' => request('service-provider')]);
            DB::commit();
            session()->flash('message', 'Successfully updated the service provider');
            return redirect('/admin/dashboard/service-providers');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/service-providers');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('service_providers')->where('id', '=', $id)->update(['deleted' => 1]);
            $deletedServiceProvider = DB::table('service_providers')->select('name')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the service provider ' . $deletedServiceProvider);
            return redirect('/admin/dashboard/service-provider');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/service-provider');
        }
    }
}
