<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use App\UserTestimonial;

class HomeController extends Controller
{
    public function index()
    {
        $userTestimonials = UserTestimonial::all()->where('deleted', '=', 0)->where('status', '=', 'published');
        return view('index', compact('userTestimonials'));
    }
}
