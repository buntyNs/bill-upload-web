<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\User;
use App\Invoice;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $billCount = count(Bill::all()->where('deleted', '=', 0));
        $userCount = count(User::all()->where('deleted', '=', 0));
        $invoiceCount = count(Invoice::all()->where('deleted', '=', 0));
        $completedInvoiceCount = count(Invoice::all()
            ->where('deleted', '=', 0)
            ->where('status', '=', 'paid')
        );
        return view('admin.index', compact('billCount', 'userCount', 'invoiceCount', 'completedInvoiceCount'));
    }
}
