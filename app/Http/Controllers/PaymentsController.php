<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payments;

class PaymentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        // $payments = Payment::all()->where('deleted', '=', 0);

        $paymentDetails = Payments::join('bills','bills.bill_no','payments.bill_id')
                                ->where('payments.deleted', '=', 0)
                                ->select("payments.id", "payments.bill_id", "payments.invoice_id","payments.payment_id","payments.amount","payments.created_at","bills.user")
                                ->get();

        return view('admin.payments.index', compact('paymentDetails'));
    }

    public function show(Payments $payment)
    {
        $user_id = $payment->user_id;
        $user = User::find($user_id);

        // $bill = Bill::find($bill);
        return view('admin.payments.show', compact('payment', 'user'));
    }


    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('payments')->where('id', '=', $id)->update(['deleted' => 1]);
            DB::commit();
            session()->flash('message', 'Successfully deleted the bill');
            return redirect('/admin/dashboard/payments');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/payments');
        }
    }
}
