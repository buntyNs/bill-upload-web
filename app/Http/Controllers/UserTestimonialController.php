<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserTestimonial;
use DB;

class UserTestimonialController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(UserTestimonial $userTestimonials)
    {
        $userTestimonials = $userTestimonials->all()->where('deleted', '=', 0);
        return view('admin.user-testimonials.index', compact('userTestimonials'));
    }

    public function create()
    {
        return view('admin.user-testimonials.create');
    }

    public function show(UserTestimonial $userTestimonial)
    {
        $userTestimonialId = $userTestimonial->id;
        $userTestimonial = UserTestimonial::find($userTestimonialId);
        return view('admin.user-testimonials.show', compact('userTestimonial'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'topic' => 'bail|required|max:50|min:10',
            'customer' => 'bail|required|max:20|min:5',
            'city' => 'bail|required|max:20|min:2',
            'body' => 'bail|required|max:500|min:10',
            'status' => 'required|in:published,unpublished'
        ]);
        DB::beginTransaction();
        try {
            $userTestimonial = UserTestimonial::create([
                'topic' => request('topic'),
                'body' => request('body'),
                'customer' => request('customer'),
                'city' => request('city'),
                'status' => request('status'),
                'deleted' => 0
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added a new user testimonial');
            return redirect('/admin/dashboard/user-testimonials');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/user-testimonials');
        }
    }

    public function edit($id)
    {
        $testimonialId = $id;
        $testimonial = UserTestimonial::find($testimonialId);
        return view('admin.user-testimonials.update', compact('testimonial'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'topic' => 'bail|required|max:50|min:10',
            'customer' => 'bail|required|max:20|min:5',
            'city' => 'bail|required|max:20|min:2',
            'body' => 'bail|required|max:500|min:10',
            'status' => 'required|in:published,unpublished'
        ]);

        DB::beginTransaction();
        try {
            DB::table('user_testimonials')->where('id', '=', $id)->update([
                'topic' => request('topic'),
                'customer' => request('customer'),
                'city' => request('city'),
                'body' => request('body'),
                'status' => request('status')
            ]);
            DB::commit();
            session()->flash('message', 'Successfully updated the user testimonial');
            return redirect('/admin/dashboard/user-testimonials');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/user-testimonials');
        }   
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('user_testimonials')->where('id', '=', $id)->update(['deleted' => 1]);
            $userTestimonialTopic = DB::table('user_testimonials')->select('topic')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the user testimonial ' . $userTestimonialTopic);
            return redirect('/admin/dashboard/user-testimonials');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/user-testimonials');
        }
    }
}
