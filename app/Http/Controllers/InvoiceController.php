<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Bill;
use App\User;
use DB;
use Mail;
use Auth;
use App\Mail\SendInvoice;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Invoice $invoices)
    {
        $invoices = $invoices->all()->where('deleted', '=', 0);
        return view('admin.manage-invoices.index', compact('invoices'));
    }

    public function create($billId, $userId)
    {
        $bill = Bill::find($billId);
        $user = User::find($userId);
        return view('admin.manage-invoices.create', compact('bill', 'user'));
    }

    public function show(Invoice $invoice)
    {
        $invoice_id = $invoice->id;
        $invoice = Invoice::find($invoice_id);
        return view('admin.manage-invoices.show', compact('invoice'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'bill-id' => 'bail|required',
            'bill-type' => 'bail|required',
            'service-provider' => 'bail|required',
            'user' => 'bail|required',
            'date' => 'bail|required|date',
            'original-amount' => 'bail|required|numeric|between:0,9999.99|gt:new-amount',
            'new-amount' => 'bail|required|numeric|between:0,9999.99|lt:original-amount'
        ]);
        
        if (count(Invoice::all()) > 0) {
            $lastInvoiceId = Invoice::all()->last()->id;
            $lastInvoiceId = $lastInvoiceId + '1';
            $invoiceNo = 'IN' . $lastInvoiceId;
        } elseif (count(Invoice::all()) == 0) {
            $invoiceNo = 'IN1';
        }
        
        $originalAmount = request('original-amount');
        $newAmount = request('new-amount');
        if ($originalAmount == $newAmount || $originalAmount < $newAmount) {
            $negotiatedAmount = 0;
        } elseif ($newAmount < $originalAmount) {
            $negotiatedAmount = $originalAmount - $newAmount;
        }
        
        if ($negotiatedAmount == 0) {
            $ourCharge = 0;
        } else {
            $ourCharge = ($negotiatedAmount / 2);
        }

        // $userId = Auth::user()->id;
        // $userId = request('user_id');
        // dd($userId);

        $payingAmount = $originalAmount - $ourCharge;
        $billId = request('id');
        $billCreatedDate = Bill::find($billId)->created_at;
        $userId = Bill::find($billId)->user_id;
        DB::beginTransaction();
        try {
            $invoice = Invoice::create([
                'invoice_no' => $invoiceNo,
                'bill_id' => request('bill-id'),
                'user_id' => $userId,
                'user' => request('user'),
                'bill_type' => request('bill-type'),
                'service_provider' => request('service-provider'),
                'bill_date' => request('date'),
                'bill_created_date' => $billCreatedDate,
                'original_amount' => request('original-amount'),
                'new_amount' => request('new-amount'),
                'negotiated_amount' => $negotiatedAmount,
                'our_charge' => $ourCharge,
                'paying_amount' => $payingAmount,
                'paypal_id' => 'jghg5465464654545545'
            ]);
            // After creating the invoice a bill's status is changed
            DB::table('bills')->where('bill_no', '=', request('bill-id'))->update(['status' => 'invoiced']);
            DB::commit();
            $email = User::find($userId)->email;
            // Mail::to($email)->send(new SendInvoice($invoice));
            session()->flash('message', 'Successfully added a new invoice');
            return redirect('/admin/dashboard/manage-invoices');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/bills');
        }
    }

    public function edit($id)
    {
        $invoice = Invoice::find($id);
        return view('admin.manage-invoices.update', compact('invoice'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'invoice-id' => 'bail|required',
            'bill-id' => 'bail|required',
            'user' => 'bail|required',
            'date' => 'bail|required|date',
            'original-amount' => 'bail|required|numeric|between:0,9999.99|gt:new-amount',
            'new-amount' => 'bail|required|numeric|between:0,9999.99|lt:original-amount'
        ]);

        $originalAmount = request('original-amount');
        $newAmount = request('new-amount');
        if ($originalAmount == $newAmount || $originalAmount < $newAmount) {
            $negotiatedAmount = 0;
        } elseif ($newAmount < $originalAmount) {
            $negotiatedAmount = $originalAmount - $newAmount;
        }
        
        if ($negotiatedAmount == 0) {
            $ourCharge = 0;
        } else {
            $ourCharge = ($negotiatedAmount / 2);
        }

        $payingAmount = $originalAmount - $ourCharge;
        
        $invoiceId = request('invoice-id');
        DB::beginTransaction();
        try {
            DB::table('invoices')->where('id', '=', $invoiceId)->update([
                'original_amount' => request('original-amount'),
                'new_amount' => request('new-amount'),
                'negotiated_amount' => $negotiatedAmount,
                'our_charge' => $ourCharge,
                'paying_amount' => $payingAmount
            ]);
            DB::commit();
            session()->flash('message', 'Successfully updated the invoice');
            return redirect('/admin/dashboard/manage-invoices/show/' . $invoiceId);
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/manage-invoices');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('invoices')->where('id', '=', $id)->update(['deleted' => 1]);
            $deletedInvoice = DB::table('invoices')->select('invoice_no')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the invoice ' . $deletedInvoice);
            return redirect('/admin/dashboard/manage-invoices');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/manage-invoices');
        }
    }
}
