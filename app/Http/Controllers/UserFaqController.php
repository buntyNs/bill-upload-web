<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;
use DB;

class UserFaqController extends Controller
{
    public function index(Faq $faqs)
    {
        $faqs = $faqs->all()->where('deleted', '=', 0)->where('status', '=', 'published');
        return view('faq', compact('faqs'));
    }

}
