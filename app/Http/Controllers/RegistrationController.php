<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\User;
use App\Country;
use App\HearAboutUs;
use App\Mail\Welcome;
use Mail;
use DB;

class RegistrationController extends Controller
{
    public function create()
    {
        $countries = Country::all();
        $hearAboutUs = HearAboutUs::all()->where('deleted', '=', 0);
        return view('registration.create', compact('countries', 'hearAboutUs'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'bail|required|max:50|min:2',
            'last_name'  => 'bail|required|max:50|min:2',
            'additional_bill_holder' => 'nullable|max:50|min:2',
            'email' => 'required|email',
            'password' => 'required|confirmed|max:15|min:8',
            'terms_and_conditions' => 'required',
            'phone_number' => 'required|numeric',
            'social_security' => 'required|numeric',
            'dob' => 'required|date',
            'street_name' => 'required|max:50|min:2',
            'street_number' => 'required|numeric',
            'city' => 'required|max:50|min:2',
            'zip_code' => 'required|numeric',
            'country' => 'required|max:50|min:2',
            'hear_about_us' => 'required',
            'promo_code' => 'nullable|max:50|min:1'
        ]);

        $hashedPassword = Hash::make(request('password'));

        $streetName = request('street_name');
        $streetNumber = request('street_number');
        $city = request('city');
        $zipCode = request('zip_code');
        $country = request('country');
        $address = $streetName . ", " . $streetNumber . ", " . $city . ", " . $zipCode . ", " . $country;
        
        DB::beginTransaction();
        try {
            $user = User::create([  
                'first_name' => request('first_name'), 
                'last_name' => request('last_name'), 
                'additional_bill_holder' => request('additional_bill_holder'),
                'email' => request('email'),
                'password' => $hashedPassword,
                'terms_and_conditions' => 1,
                'phone_number' => request('phone_number'),
                'social_security' => request('social_security'),
                'dob' => request('dob'),
                'street_name' => request('street_name'),
                'street_number' => request('street_number'),
                'city' => request('city'),
                'zip_code' => request('zip_code'),
                'country' => request('country'),
                'hear_about_us' => request('hear_about_us'),
                'promo_code' => request('promo_code'),
                'address' => $address,
                'role' => 'user',
                'deleted' => 0
            ]);

            // dd($user->id);

            // DB::table('role_user')->insert(
            //     ['role_id' => 2, 'user_id' => $user->id]
            // );

            DB::commit();
            auth()->login($user);
            // Mail::to($user)->send(new Welcome($user));
            session()->flash('message', 'Thanks so much for signing up!');
            return redirect('/dashboard');
        } catch (\PDOException $e) {
            DB::rollback();
            // dd($e);
            if ($e->errorInfo[1] == 1062) {
                $errorCode = $e->errorInfo[1];
                session()->flash('message-error-email', 'You entered email is already taken');    
            }
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/register');
        }
    }
}
