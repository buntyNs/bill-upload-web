<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillType;
use DB;

class BillTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(BillType $billTypes)
    {
        $billTypes = $billTypes->all()->where('deleted', '=', 0);
        return view('admin.bill-types.index', compact('billTypes'));
    }

    public function create()
    {
        return view('admin.bill-types.create');
    }

    public function update($id)
    {
        DB::beginTransaction();
        try {
            DB::table('bill_types')->where('id', '=', $id)->update(['name' => request('bill-type')]);
            DB::commit();
            session()->flash('message', 'Successfully updated the bill type');
            return redirect('/admin/dashboard/bill-types');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/bill-types');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'bill-type' => 'bail|required|max:20|min:3'
        ]);

        DB::beginTransaction();
        try {
            $billType = BillType::create([
                'name' => request('bill-type'),
                'deleted' => 0
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added the new bill type');
            return redirect('/admin/dashboard/bill-types');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/bill-types');
        }
    }

    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            DB::table('bill_types')->where('id', '=', $id)->update(['deleted' => 1]);
            $deletedBillType = DB::table('bill_types')->select('name')->where('id', '=', $id)->get();
            DB::commit();
            session()->flash('message', 'Successfully deleted the bill type ' . $deletedBillType);
            return redirect('/admin/dashboard/bill-types');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/bill-types');
        }
    }
}
