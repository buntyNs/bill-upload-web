<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use DB;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index(Country $country)
    {
        $countries = $country->all();
        return view('admin.country.index', compact('countries'));
    }

    public function create()
    {
        return view('admin.service-provider.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'bail|required|max:50|min:3'
        ]);
        DB::beginTransaction();
        try {
            $country = Country::create([
                'name' => request('name')            
            ]);
            DB::commit();
            session()->flash('message', 'Successfully added a new country');
            return redirect('/admin/dashboard/country');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/country');
        }
    }

    public function update($id)
    {
        DB::beginTransaction();
        try {
            DB::table('countries')->where('id', '=', $id)->update(['name' => request('name')]);
            DB::commit();
            session()->flash('message', 'Successfully updated the country');
            return redirect('/admin/dashboard/country');
        } catch (\Exception $e) {
            DB::rollback();
            session()->flash('message-error', 'Something went wrong, please try again');
            return redirect('/admin/dashboard/country');
        }
    }
}
