<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first-name' => 'bail|required',
            'last-name'  => 'bail|required',
            'additional-bill-holder' => 'nullable|min:2',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'terms-and-conditions' => 'required',
            'phone-number' => 'required|max:12',
            'social-security' => 'required|max:4',
            'dob' => 'required|date',
            'street-name' => 'required',
            'street-number' => 'required',
            'city' => 'required',
            'zip-code' => 'required',
            'country' => 'required',
            'hear-about-us' => 'required',
            'promo-code' => 'required'
        ];
    }

    public function persist()
    {
        // $id = DB::table('users')->insertGetId([
        //     'first_name' => request('first-name'),
        //     'last_name' => request('last-name'),
        //     'additional_bill_holder' => request('additional-bill-holder'),
        //     'email' => request('email'),
        //     'password' => Hash::make(request('password')),
        //     'terms_and_conditions' => 1,
        //     'phone_number' => request('phone-number'),
        //     'social_security' => request('social-security'),
        //     'dob' => request('dob'),
        //     'street_name' => request('street-name'),
        //     'street_number' => request('street-number'),
        //     'city' => request('city'),
        //     'zip_code' => request('zip-code'),
        //     'country' => request('country'),
        //     'hear_about_us' => request('hear-about-us'),
        //     'promo_code' => request('promo-code'),
        //     'address' => "street-name" . "street-number" . "city" . "zip-code" . "country",
        //     'deleted' => 0
        // ]);

        $user = User::create(
            $this->only([
                'first-name',
                'last-name',
                'additional-bill-holder',
                'email',
                'password' => Hash::make(request('password')),
                'terms-and-conditions' => 1,
                'phone-number',
                'social-security',
                'dob',
                'street-name',
                'street-number',
                'city',
                'zip-code',
                'country',
                'hear-about-us',
                'promo-code',
                'address' => "street-name" . "street-number" . "city" . "zip-code" . "country",
                'deleted' => 0
            ])
        );
        auth()->login($user);
    }
}
