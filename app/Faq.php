<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    protected $fillable = [
        'topic',
        'body',
        'status',
        'deleted'
    ];

    protected $attributes = [
        'status' => 'unpublished'
    ];
}
