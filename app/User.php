<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $guarded = [
        
    // ];
    protected $fillable = [
        'first_name',
        'last_name',
        'additional_bill_holder',
        'email',
        'password',
        'terms_and_conditions',
        'phone_number',
        'social_security',
        'dob',
        'street_name',
        'street_number',
        'city',
        'zip_code',
        'country',
        'hear_about_us',
        'promo_code',
        'address',
        'role',
        'deleted'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this
            ->belongsToMany('App\Role')
            ->withTimestamps();
    }

    public function bills()
    {
        return $this->hasMany(Bill::class);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function hasRole($role) {
        if ($role) {
            return $this->role == $role;
        }
        return !! $this->role;
    }

    // public function authorizeRoles($roles)
    // {
    //     if ($this->hasAnyRole($roles)) {
    //         return true;
    //     }
    //     abort(401, 'This action is unauthorized.');
    // }

    // public function hasAnyRole($roles)
    // {
    //     if (is_array($roles)) {
    //         foreach ($roles as $role) {
    //         if ($this->hasRole($role)) {
    //             return true;
    //         }
    //         }
    //     } else {
    //         if ($this->hasRole($roles)) {
    //         return true;
    //         }
    //     }
    //     return false;
    // }

    // public function hasRole($role)
    // {
    //     if ($this->roles()->where('name', $role)->first()) {
    //         return true;
    //     }
    //     return false;
    // }
}
