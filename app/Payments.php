<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
        'bill_id',
        'invoice_id',
        'user_id',
        'transaction_id',
        'payment_id',
        'amount'
    ];

    protected $attributes = [
        'deleted' => 0
    ];
}
