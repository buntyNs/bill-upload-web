<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTestimonial extends Model
{
    protected $fillable = [
        'topic',
        'body',
        'customer',
        'city',
        'status',
        'deleted'
    ];

    protected $attributes = [
        'status' => 'unpublished'
    ];
}
