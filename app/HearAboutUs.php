<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HearAboutUs extends Model
{
    protected $fillable = [
        'name',
        'deleted'
    ];
}
