@extends('layouts.master-log')

@section('content')
<!-- Wizard container -->
<div class="wizard-container">

    <!-- You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red" -->
    <div class="card wizard-card" data-color="orange" id="wizardProfile">

        <form action="/dashboard/account-settings" method="POST">
            @csrf
            <div class="wizard-header">
                <h3>
                    <b>EDIT</b> MY ACCOUNT <br>
                    {{-- <small>This information will let us know more about you</small> --}}
                </h3>
            </div>

            <div class="wizard-navigation">
                <ul>
                    <li><a href="#primary" data-toggle="tab">Account Settings</a></li>
                    {{-- <li><a href="#secondary" data-toggle="tab">Secondary Details</a></li> --}}
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane" id="primary">
                    <div class="row">
                        <h4 class="info-text">Please update the details</h4>

                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label for="email">Email Address <small>*</small></label>
                                <input name="email" type="email" id="email" class="form-control" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="col-sm-10 col-sm-offset-1"> 
                            <div class="form-group">
                                <label for="password">New Password <small>*</small></label>
                                <input name="password" type="password" id="password" class="form-control" required>
                            </div>
						</div>
						<div class="col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label for="password-confirmation">Password Confirmation <small>*</small></label>
                                <input name="password_confirmation" type="password" id="password-confirmation" class="form-control" required>
                            </div>
						</div>
                    </div>
                </div>
            </div>
            <div class="wizard-footer height-wizard">
                <div class="pull-right">
                    <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Update' />
                </div>
                <div class="clearfix"></div>
            </div>

        </form>
    </div>

    @include('layouts.errors')

</div> <!-- wizard container -->
@endsection
