@extends('layouts.master-log')

@section('content')
<!-- Wizard container -->
<div class="wizard-container">

    <!-- You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red" -->
    <div class="card wizard-card" data-color="orange" id="wizardProfile">

        <form action="/dashboard/edit-my-account" method="POST">
            @csrf
            <div class="wizard-header">
                <h3>
                    <b>EDIT</b> MY PROFILE <br>
                    {{-- <small>This information will let us know more about you</small> --}}
                </h3>
            </div>

            <div class="wizard-navigation">
                <ul>
                    <li><a href="#primary" data-toggle="tab">Primary Details</a></li>
                    {{-- <li><a href="#secondary" data-toggle="tab">Secondary Details</a></li> --}}
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane" id="primary">
                    <div class="row">
                        <h4 class="info-text">Please update the details</h4>

                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label for="first-name">First Name <small>*</small></label>
                                <input name="first_name" type="text" id="first-name" class="form-control" value="{{ $accountDetails->first_name }}">
                            </div>
                        </div>
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label for="last-name">Last Name <small>*</small></label>
                                <input name="last_name" type="text" id="last-name" class="form-control" value="{{ $accountDetails->last_name }}">
                            </div>
                        </div>
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label for="additional-bill-holder">Additional contact / Bill holder</label>
                                <input name="additional_bill_holder" type="text" id="additional-bill-holder" class="form-control" value="{{ $accountDetails->additional_bill_holder }}">
                            </div>
                        </div>

                        <div class="col-sm-6 col-sm-offset-1">
                            <div class="form-group">
                                <label for="phone-number">Phone Number <small>*</small></label>
                                <input type="text" id="phone-number" class="form-control" name="phone_number" value="{{ $accountDetails->phone_number }}">
                            </div>
                        </div>
        
                        <div class="col-sm-7 col-sm-offset-1">
                            <div class="form-group">
                                <label for="street-name">Street Name <small>*</small></label>
                                <input type="text" id="street-name" class="form-control" name="street_name" value="{{ $accountDetails->street_name }}">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="street-number">Street Number <small>*</small></label>
                                <input type="text" id="street-number" class="form-control" name="street_number" value="{{ $accountDetails->street_number }}">
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <div class="form-group">
                                <label for="city">City <small>*</small></label>
                                <input type="text" id="city" class="form-control" name="city" value="{{ $accountDetails->city }}">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label for="zip-code">Zip Code <small>*</small></label>
                                <input type="text" id="zip-code" class="form-control" name="zip_code" value="{{ $accountDetails->zip_code }}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="country">
                                    Country <small>*</small>
                                </label><br>
                                <select id="country" class="form-control" name="country">
                                    @foreach ($countries as $country)
                                    <option value="{{ $country->name }}" @if (($country->name) == ($accountDetails->country)) selected @endif> {{ $country->name }} </option>
                                    @endforeach
                                    {{-- <option value="{{ $accountDetails->country }}"> {{ $accountDetails->country }} </option> --}}
                                </select>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="wizard-footer height-wizard">
                <div class="pull-right">
                    <input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Update' />
                </div>
                <div class="clearfix"></div>
            </div>

        </form>
    </div>

    @include('layouts.errors')

</div> <!-- wizard container -->
@endsection
