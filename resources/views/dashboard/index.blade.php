@extends('layouts.master-dashboard')

@section('content')
<section id="iq-home" class="iq-banner-08 overview-block-pt grey-bg" style="background-color:#17a2b830">
    <div class="container">
        <div class="banner-text">
            <div class="row">
                <!-- <div class="col-lg-6">
                    <h1 class="text-uppercase iq-font-blue iq-tw-3">We are building <b class="iq-tw-7">software</b> to help</h1>
                    <p class="iq-font-black iq-pt-15 iq-mb-40">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley, </p>
                    <a href="javascript:void(0)" class="button-blue-shadow iq-mt-20 iq-mr-20">Learn More</a>
                    <a href="javascript:void(0)" class="button-line-shadow iq-mt-20">Download</a>
                </div> -->
                <h1 id="clientName" class="text-uppercase iq-font-blue iq-tw-3"></h1>
            </div>
        </div>
    </div>
</section>
<!-- Banner End -->

    <!-- Main Content -->
    <div class="main-content">
        <section id="great-features" class="life-work-1 overview-block-pt software" style="margin-bottom:30px;padding: 20px">
            <div class="container-form">
                <div class="sidebar">
                    <a class="active" href="/dashboard">Dashboard</a>
                    <a href="/dashboard/add-my-bill">Add a Bill</a>
                    <a id="editAccount" href="/dashboard/edit-my-account">Edit My Account</a>
                    <a id="invoices" href="/dashboard/invoices">Invoices</a>
                    <a id="accountSettings" href="/dashboard/account-settings">Account Settings</a>
                    <a id="logout" href="/logout">Logout</a>
                </div>

                <div id="bodydiv" class="content">
                    <div class="container-fluid">
                        <ul class="responsive-table">
                            <li class="table-header">
                                <div class="col col-2">ID</div>
                                <div class="col col-3">Bill Type</div>
                                <div class="col col-3">Service Provider</div>
                                <div class="col col-4">Status</div>
                            </li>
                            @if (count($uploadedBills) == 0 && count($invoicedBills) == 0)
                            <li class="table-row">
                                <div class="col col-12 text-center" data-label="">Currently there are no bills to show, <a href="/dashboard/add-my-bill">add new bill</a></div>
                            </li>
                            @endif
                            @foreach ($uploadedBills as $uploadedBill)
                            <li class="table-row">
                                <div class="col col-2" data-label="Bill No">{{ $uploadedBill->bill_no }}</div>
                                <div class="col col-3" data-label="Customer Name">{{ ucwords($uploadedBill->bill_type) }}</div>
                                <div class="col col-3" data-label="Service Provider">{{ $uploadedBill->service_provider }}</div>
                                <div class="col col-4" data-label="Payment Status">{{ ucfirst($uploadedBill->status) }}</div>
                            </li>
                            @endforeach

                            @foreach ($invoicedBills as $invoicedBill)
                            <li class="table-row">
                                <div class="col col-2" data-label="Bill No">{{ $invoicedBill->bill_no }}</div>
                                <div class="col col-3" data-label="Customer Name">{{ ucwords($invoicedBill->bill_type) }}</div>
                                <div class="col col-3" data-label="Service Provider">{{ $invoicedBill->service_provider }}</div>
                                <div class="col col-4" data-label="Payment Status">
                                    @if ($invoicedBill->status == 'invoiced')
                                    <div>
                                        <form  style = "display : flex" action="/dashboard/bill-payment/paypal" method="POST">
                                            @csrf
                                            <p>{{ $invoicedBill->paying_amount }}</p>
                                            <input type="hidden" name="amount" value="{{ $invoicedBill->paying_amount }}">
                                            <button class = "pay-button" type="submit">Pay Now</button>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>

    @if ($flash = session('message'))  
        <div class="alert alert-success text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
            {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
            {{ $flash }}
        </div>
    @endif
    @if ($flash = session('message-error'))  
        <div class="alert alert-danger text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
            {{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
            {{ $flash }}
        </div>
    @endif

@endsection 
