@extends('layouts.master-dashboard')

@section('content')

<section id="iq-home" class="iq-banner-08 overview-block-pt grey-bg" style="background-color:#17a2b830">
        <div class="container">
            <div class="banner-text">
                <div class="row">
                    <!-- <div class="col-lg-6">
                        <h1 class="text-uppercase iq-font-blue iq-tw-3">We are building <b class="iq-tw-7">software</b> to help</h1>
                        <p class="iq-font-black iq-pt-15 iq-mb-40">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley, </p>
                        <a href="javascript:void(0)" class="button-blue-shadow iq-mt-20 iq-mr-20">Learn More</a>
                        <a href="javascript:void(0)" class="button-line-shadow iq-mt-20">Download</a>
                    </div> -->
                    <h1  id = "clientName" class="text-uppercase iq-font-blue iq-tw-3">Invoices</h1>


                </div>
            </div>
        </div>
    </section>
    <!-- Banner End -->
    <!-- Main Content -->
    <div class="main-content">
        <section id="great-features" class="life-work-1 overview-block-pt software"
            style="margin-bottom:30px;padding: 20px">
            <div class="container-form">
                <div class="sidebar">
                    <a href="/dashboard">Dashboard</a>
                    <a href="/dashboard/add-my-bill">Add a Bill</a>
                    <a id="logout" href="/dashboard/edit-my-account">Edit My Account</a>
                    <a id="logout" class="active" href="/dashboard/invoices">Invoices</a>
                    <a id="logout" href="/dashboard/account-settings">Account Settings</a>
                    <a id="logout" href="/logout">Logout</a>
                </div>

                <div id="bodydiv" class="content">
                    <!-- {#<div>#}
                        {#<h3>You have no bookings yet ! </h3>#}
                        {#<img id = "bookingImg" src = "/images/bg/booking.png"/>#}
                    {#</div>#}
                {# need to loop start #} -->


                <div class="container-fluid">
                    <ul class="responsive-table">
                        <li class="table-header">
                            <div class="col col-2">ID</div>
                            <div class="col col-3">Bill Type</div>
                            <div class="col col-3">Service Provider</div>
                            {{-- <div class="col col-2">Amount Due</div> --}}
                            <div class="col col-4">Status</div>
                        </li>
                        @if (count($paidInvoices) == 0)
                        <li class="table-row">
                            <div class="col col-12 text-center" data-label="">Currently there are no invoices that you've paid.</div>
                        </li>
                        @endif
                        @foreach ($paidInvoices as $paidInvoice)    
                        <li class="table-row">
                            <div class="col col-2" data-label="Bill No">{{ $paidInvoice->bill_id }}</div>
                            <div class="col col-3" data-label="Customer Name">{{ ucwords($paidInvoice->bill_type) }}</div>
                            <div class="col col-3" data-label="Service Provider">{{ $paidInvoice->service_provider }}</div>
                            {{-- <div class="col col-2" data-label="Amount"></div> --}}
                            <div class="col col-4" data-label="Payment Status">{{ $paidInvoice->status }}</div>
                        </li>
                        @endforeach
                    </ul>
                </div>

                    
        <!-- {# need to loop end #} -->
  
                </div>
            </div>

        </section>

    </div>



<!-- <section id="iq-services" class="iq-counter-box-1 overview-block-ptb it-works re4-mt-50 iq-font-black text-center">
    <div class="container">
        <div class="row iq-mt-100">
            <div class="col-sm-12">
                <div class="heading-title">
                    <h5 class="iq-tw-1">
                        Welcome to your bill saving dashboard, where you can check the status of your bills, see how much you've saved and add more bills to your account. If you have any questions, we're always happy to help.
                    </h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Account Status</h5>
                    <p class="iq-mb-15">Canceled</p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 r-mt-30">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Customer Since</h5>
                    <p class="iq-mb-15">22/05/2019</p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 r-mt-30">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Your Savings Officer</h5>
                    <p class="iq-mb-15">John Doe</p>
                </div>
            </div>
        </div>
        <div class="row iq-mt-0">
                <div class="col-sm-12 col-lg-3">
                    <div class="iq-works-box no-shadow text-center">
                        <a href="/dashboard/add-my-bill" class="button">Add a Bill</a>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 r-mt-30">
                    <div class="iq-works-box no-shadow text-center">
                        <a href="/dashboard/edit-my-account" class="button">Edit Your Account</a>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 r-mt-30">
                    <div class="iq-works-box no-shadow text-center">
                        <a href="javascript:void(0)" class="button">Your Invoices</a>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 r-mt-30">
                    <div class="iq-works-box no-shadow text-center">
                        <a href="javascript:void(0)" class="button">Contact Us</a>
                    </div>
                </div>
            </div>
    </div>
</section>

<section id="iq-services" class="iq-counter-box-1 overview-block-ptb it-works re4-mt-50 iq-font-black text-center">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-4">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Account Status</h5>
                    <p class="iq-mb-15">Canceled</p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 r-mt-30">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Customer Since</h5>
                    <p class="iq-mb-15">22/05/2019</p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-4 r-mt-30">
                <div class="iq-works-box no-shadow text-center">
                    <h5 class="iq-tw-2 text-uppercase iq-mt-25 iq-mb-15">Your Savings Officer</h5>
                    <p class="iq-mb-15">John Doe</p>
                </div>
            </div>
        </div>
    </div>
</section> -->

@endsection 
