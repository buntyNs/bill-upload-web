<!-- <html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add a Bill</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <h1 class="text-center">Add a Bill</h1><br>
    <div class="container">
        <form method="POST" action="/dashboard/add-my-bill" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="type-of-bill">Type of Bill</label>
                    <select id="type-of-bill" class="form-control" name="type-of-bill">
                        <option value="">--Please choose an option--</option>
                        <option value="cell-phone">Cell Phone</option>
                        <option value="satelite">Satelite</option>
                        <option value="internet">Internet</option>
                        <option value="landline">Landline</option>
                        <option value="electricity">Electricity</option>
                        <option value="water">Water</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="service-provider">Service Provider</label>
                    <select id="service-provider" class="form-control" name="service-provider">
                        <option value="">--Please choose an option--</option>
                        <option value="ceb">CEB</option>
                        <option value="satelite">Mobitel</option>
                        <option value="internet">Dialog</option>
                        <option value="landline">Airtel</option>
                        <option value="electricity">SLT</option>
                        <option value="other">Other</option>
                    </select>
                </div>
                <div class="col-md-4 mb-3">
                    <label for="duration-of-service">How long have you had this service?</label>
                    <div class="input-group">
                        <select id="duration-of-service" class="form-control" name="duration-of-service">
                            <option value="">--Please choose an option--</option>
                            <option value="<1">Less than one year</option>
                            <option value="1-2">1 - 2 Years</option>
                            <option value="3-5">3 - 5 Years</option>
                            <option value="5>">5+ Years</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-4 mb-3">
                    <label for="bill-upload">Upload your latest statement</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" id="bill-upload" class="custom-file-input" name="bill-upload" accept="image/*,.pdf">
                            <label for="bill-upload" class="custom-file-label">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <label for="bill-date">What is the date of this bill?</label>
                    <input type="date" id="bill-date" class="form-control" name="bill-date" placeholder="">
                </div>
            </div>

            <div class="form-group">
                <div class="form-check">
                    <input type="checkbox" id="renew-service" class="form-check-input" name="renew-service">
                    <label class="form-check-label" for="renew-service">
                        Would you be interested in renewing or extending a contract with your provider to save money?
                    </label>
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-5 mb-3">
                    <label for="passcode">Enter your account security passcode if you have one</label>
                    <input type="text" id="passcode" class="form-control" name="passcode" placeholder="">
                </div>
            </div>

            <div class="form-row">
                <div class="col-md-5 mb-3">
                    <label for="bill-comment">
                        Is there anything else we should know about this bill?
                    </label>
                    <textarea id="bill-comment" class="form-control" name="bill-comment" rows="3"></textarea>
                </div>
            </div>

            <button class="btn btn-primary" type="submit">Add the bill</button>
        </form>

        @include('layouts.errors')

</body>
</html> -->


@extends ('layouts.master-log')

    @section ('content')
			
	<!-- Wizard container -->
	<div class="wizard-container">

		<!-- You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red" -->
		<div class="card wizard-card" data-color="orange" id="wizardProfile">

			<form  method="POST" action="/dashboard/add-my-bill" enctype="multipart/form-data">
				@csrf
				<div class="wizard-header">
					<h3>
						<b>ADD</b> YOUR BILL <br>
					</h3>
				</div>

				<div class="">
					<ul>
						<li><a href="#primary" data-toggle="tab">Bill Details</a></li>
					</ul>
				</div>

				<div class="tab-content">
					<div class="tab-pane" id="primary">
						<div class="row">
                            <h4 class="info-text">Please fill in the details</h4>
                            
                            @include('layouts.errors')

							<div class="col-sm-10 col-sm-offset-1">
							<div class="form-group">
									<label for="type-of-bill">Type of Bill</label><br>
									<select id="type-of-bill" class="form-control" name="type-of-bill" required>
                                        <option value=""> -- select an option -- </option>
                                        @foreach ($billTypes as $billType)
                                            <option value="{{ $billType->name }}">{{ ucfirst($billType->name) }}</option>
                                        @endforeach
									</select>
								</div>
							</div>

							<div class="col-sm-10 col-sm-offset-1">
							    <div class="form-group">
                                    <label for="service-provider">Service Provider</label><br>
                                    <select id="service-provider" class="form-control" name="service-provider" required>
                                        <option value=""> -- select an option -- </option>
                                        @foreach ($serviceProviders as $serviceProvider)
                                            <option value="{{ $serviceProvider->name }}">{{ ucfirst($serviceProvider->name) }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>

							<div class="col-sm-10 col-sm-offset-1">
							    <div class="form-group">
									<label for="duration-of-service">How long have you had this service?</label><br>
									<select id="duration-of-service" class="form-control" name="duration-of-service" required>
                                        <option value=""> -- select an option --</option>
                                        <option value="less than one year">Less than one year</option>
                                        <option value="between one and two years">1 - 2 Years</option>
                                        <option value="between three and five years">3 - 5 Years</option>
                                        <option value="over five years">5+ Years</option>
                                    </select>
								</div>
							</div>

							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="bill-date">What is the date of this bill? </label>
									<input name="bill-date" type="date" id="bill-date" class="form-control" required>
								</div>
							</div>

							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<input name="renew-service" type="checkbox" id="renew-service" required>
									<label for="terms-and-conditions">Would you be interested in renewing or extending a contract with your provider to save money?</label>
								</div>
							</div>

							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="passcode">Enter your account security passcode if you have one</label>
									<input name="passcode" type="password" id="passcode" class="form-control">
								</div>
						    </div>

						    <div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="password">Is there anything else we should know about this bill?</label>
									<textarea id="bill-comment" class="form-control" name="bill-comment" rows="3"></textarea>
								</div>
						    </div>

						    <div class="col-sm-10 col-sm-offset-1">
								<div class="input-group">
									<div class="custom-file">
										<input type="file" id="bill-upload" class="custom-file-input" name="bill-upload" accept="image/*,.pdf" required>
									</div>
                   				 </div>
							</div>
					</div>
				</div>
				<div class="wizard-footer height-wizard">
					<div class="pull-right">
						<button type='submit' class='btn  btn-fill btn-warning btn-wd btn-sm'>Add The Bill</button>
					</div>
					<div class="clearfix"></div>
				</div>
			</form>
		</div>

		

	</div> <!-- wizard container -->
    @endsection

