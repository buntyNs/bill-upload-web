@extends ('layouts.master')

@section ('content')
<section id="iq-home" class="iq-banner-08 overview-block-pt grey-bg" style="background-color:#17a2b830">
        <div class="container">
            <div class="banner-text">
                <div class="row">
                    <h1 class="text-uppercase iq-font-blue iq-tw-3">Frequently </h1>
                    <p class="iq-font-black iq-pt-15 iq-mb-40">
                    Asked  Questions</p>
                </div>
            </div>
        </div>
 </section>

<div class="main-content">
    <section class="overview-block-ptb white-bg iq-asked">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="iq-accordion iq-mt-50">

                        
                        {{-- <div class="iq-ad-block ad-active">
                            <a href="javascript:void(0)" class="ad-title iq-tw-6 iq-font-grey"></a>
                            <div class="ad-details">
                                <div class="row">
                                    <div class="col-sm-3"><img alt="#" class="img-fluid" src="images/blog/01.jpg"></div>
                                    <div class="col-sm-9">
                                        
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        




                        @foreach ($faqs as $faq)
                        <div class="iq-ad-block"> 
                            <a href="javascript:void(0)" class="ad-title iq-tw-6 iq-font-grey">{{ $faq->topic }}</a>
                            <div class="ad-details">{!! $faq->body !!}</div>
                        </div>
                        @endforeach



                        {{-- <div class="iq-ad-block"> <a href="javascript:void(0)" class="ad-title iq-tw-6 iq-font-grey">Standard dummy since the 1500s?</a>
                            <div class="ad-details">
                                <div class="row">
                                    <div class="col-sm-9"> It has survived not only five centuries, but also the leap into electronic typesetting. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur.</div>
                                    <div class="col-sm-3"><img alt="#" class="img-fluid" src="images/blog/01.jpg"></div>
                                </div>
                            </div>
                        </div> --}}
                        {{-- <div class="iq-ad-block"> <a href="javascript:void(0)" class="ad-title iq-tw-6 iq-font-grey">It has survived five centuries?</a>
                            <div class="ad-details">It has survived not only five centuries, but also the leap into electronic typesetting. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.</div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="iq-objects-asked">
            <span class="iq-objects-01">
                <img src="images/drive/02.png" alt="drive02">
            </span>   
        </div>
    </section>
</div>
@endsection
