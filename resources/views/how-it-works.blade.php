@extends ('layouts.master')


@section ('content')
<section id="iq-home" class="iq-banner-08 overview-block-pt grey-bg" style="background-color:#17a2b830">
        <div class="container">
            <div class="banner-text">
                <div class="row">
                    <h1 class="text-uppercase iq-font-blue iq-tw-3">How it </h1>
                    <p class="iq-font-black iq-pt-15 iq-mb-40">
                    Works.</p>

                </div>
            </div>
        </div>
 </section>

 <div class="main-content">
 <section id="how-it-works" class="overview-block-ptb it-works">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">How it Works</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley, </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">1</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Best Price</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 r-mt-30">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">2</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Adjustable</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 r-mt-30">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">3</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Years of Warranty</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</div>

        @endsection