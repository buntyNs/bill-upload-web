@extends ('layouts.master-log')

    @section ('content')
			
	<!-- Wizard container -->
	<div class="wizard-container">

		<!-- You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red" -->
		<div class="card wizard-card" data-color="orange" id="wizardProfile">

			<form action="/register" method="post">
				@csrf
				<div class="wizard-header">
					<h3>
						<b>BUILD</b> YOUR PROFILE <br>
						<small>This information will let us know more about you</small>
					</h3>
				</div>

				<div class="wizard-navigation">
					<ul>
						<li><a href="#primary" data-toggle="tab">Primary Details</a></li>
						<li><a href="#secondary" data-toggle="tab">Secondary Details</a></li>
					</ul>
				</div>

				<div class="tab-content">
					<div class="tab-pane" id="primary">
						<div class="row">
							<h4 class="info-text">Please fill in the details</h4>
							@include('layouts.errors')
							@if ($flash = session('message-error-email'))  
								<div style="margin-left:4px; margin-right:4px" class="alert alert-danger text-center" role="alert">
									{{-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
									{{ $flash }}
								</div>
    						@endif
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="first-name">First Name <small>*</small></label>
									<input name="first_name" type="text" id="first-name" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="last-name">Last Name <small>*</small></label>
									<input name="last_name" type="text" id="last-name" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="additional-bill-holder">Additional contact / Bill holder</label>
									<input name="additional_bill_holder" type="text" id="additional-bill-holder" class="form-control">
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="email">Email Address <small>*</small></label>
									<input name="email" type="email" id="email" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="password">Password <small>*</small></label>
									<input name="password" type="password" id="password" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="password-confirmation">Password Confirmation <small>*</small></label>
									<input name="password_confirmation" type="password" id="password-confirmation" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-10">
								<div class="col-sm-10 col-sm-offset-1">
									<input name="terms_and_conditions" type="checkbox" id="terms-and-conditions" required>
									<label for="terms-and-conditions">I Agree With the Terms and Conditions <small>*</small></label>
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane" id="secondary">

						<div class="row">
							
							<div class="col-sm-6 col-sm-offset-1">
								<div class="form-group">
									<label for="phone-number">Phone Number <small>*</small></label>
									<input type="text" id="phone-number" class="form-control" name="phone_number" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="social-security">Last 4 of your social security <small>*</small></label>
									<input type="text" id="social-security" class="form-control" name="social_security" required>
								</div>
							</div>
							<div class="col-sm-6 col-sm-offset-1">
								<div class="form-group">
									<label for="dob">Date of Birth <small>*</small></label>
									<input type="date" id="dob" class="form-control" name="dob" required>
								</div>
							</div>
							
						</div>

						<div class="row">
							<div class="col-sm-7 col-sm-offset-1">
								<div class="form-group">
									<label for="street-name">Street Name <small>*</small></label>
									<input type="text" id="street-name" class="form-control" name="street_name" required>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="form-group">
									<label for="street-number">Street Number <small>*</small></label>
									<input type="text" id="street-number" class="form-control" name="street_number" required>
								</div>
							</div>
							<div class="col-sm-4 col-sm-offset-1">
								<div class="form-group">
									<label for="city">City <small>*</small></label>
									<input type="text" id="city" class="form-control" name="city" required>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label for="zip-code">Zip Code <small>*</small></label>
									<input type="text" id="zip-code" class="form-control" name="zip_code" required>
								</div>
							</div>
							<div class="col-sm-4">
								<div class="form-group">
									<label for="country">
										Country <small>*</small>
									</label><br>
									<select id="country" class="form-control" name="country" required>
										<option value=""> -- select an option -- </option>
										@foreach ($countries as $country)
										<option value="{{ $country->name }}"> {{ $country->name }} </option>
										@endforeach
									</select>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="col-sm-5 col-sm-offset-1">
								<label for="hear-about-us">
									How did you hear about us <small>*</small>
								</label><br>
								<select id="hear-about-us" class="form-control" name="hear_about_us" required>
									<option value=""> -- select an option -- </option>
									@foreach ($hearAboutUs as $hearAbout)
									<option value="{{ $hearAbout->name }}"> {{ $hearAbout->name }} </option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-5">
								<div class="form-group">
									<div class="form-group">
										<label for="promo-code">Promo Code <small>*</small></label>
										<input type="text" id="promo-code" class="form-control" name="promo_code">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="wizard-footer height-wizard">
					<div class="pull-right">
						<input type='button' class='btn btn-next btn-fill btn-warning btn-wd btn-sm' name='next' value='Next' />
						<input type='submit' class='btn btn-finish btn-fill btn-warning btn-wd btn-sm' name='finish' value='Finish' />
					</div>

					<div class="pull-left">
						<input type='button' class='btn btn-previous btn-fill btn-default btn-wd btn-sm' name='previous' value='Previous' />
					</div>
					<div class="clearfix"></div>
				</div>

			</form>
		</div>

	</div> <!-- wizard container -->
    @endsection
