<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="/admin/dashboard">Admin Dashboard</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Notifications -->
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                        <i class="material-icons">notifications</i>
                        <span class="label-count">{{ $billNotification }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">NOTIFICATIONS</li>
                        <li class="body">
                            <ul class="menu">
                                <?php
                                    $bills = $billNotifications;
                                    $count = 0;
                                ?>
                                @foreach ($bills as $bill)
                                <li>
                                    <a href="/admin/dashboard/bills/show/{{ $bill->id }}">
                                        <div class="icon-circle bg-cyan">
                                            <i class="material-icons">add_shopping_cart</i>
                                        </div>
                                        <div class="menu-info">
                                            <h4>{{ ucfirst($bill->bill_type) }} bill added</h4>
                                            <p>
                                                <i class="material-icons">access_time</i> {{ $bill->created_at->diffForHumans() }}
                                            </p>
                                        </div>
                                    </a>
                                </li>
                                <?php $count++; ?>
                                <?php if ($count == 5) break; ?>
                                @endforeach
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="/admin/dashboard/bills">View All Bills</a> 
                        </li>
                    </ul>
                </li>
                <!-- #END# Notifications -->
                
                <li class="drop-down">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">more_vert</i>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="/admin/logout"><i class="material-icons">exit_to_app</i>Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
