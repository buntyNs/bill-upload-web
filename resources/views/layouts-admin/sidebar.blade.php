<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        {{-- <div class="user-info">
            <div class="image">
                <img src="/admin/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Administrator</div>
                <div class="email">admin@example.com</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div> --}}
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active">
                    <a href="/admin/dashboard">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/manage-users">
                        <i class="material-icons">contacts</i>
                        <span>Manage Users</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/manage-invoices">
                        <i class="material-icons">layers</i>
                        <span>Manage Invoices</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/bills">
                        <i class="material-icons">attach_money</i>
                        <span>Bills</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/payments">
                        <i class="material-icons">monetization_on</i>
                        <span>Payments</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/bill-types">
                        <i class="material-icons">widgets</i>
                        <span>Bill Types</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/service-providers">
                        <i class="material-icons">business</i>
                        <span>Service Providers</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/hear-about-us">
                        <i class="material-icons">hearing</i>
                        <span>Hear About Us</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/faq">
                        <i class="material-icons">question_answer</i>
                        <span>FAQ</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/user-testimonials">
                        <i class="material-icons">feedback</i>
                        <span>User Testimonials</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/dashboard/country">
                        <i class="material-icons">flag</i>
                        <span>Countries</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2019 <a href="javascript:void(0);">All Rights Reserved</a>.
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
