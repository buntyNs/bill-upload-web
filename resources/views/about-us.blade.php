@extends ('layouts.master')


@section ('content')
<section id="iq-home" class="iq-banner-08 overview-block-pt grey-bg" style="background-color:#17a2b830">
        <div class="container">
            <div class="banner-text">
                <div class="row">
                <h1 class="text-uppercase iq-font-blue iq-tw-3">About</h1>
                    <p class="iq-font-black iq-pt-15 iq-mb-40">
                        our work.</p>


                </div>
            </div>
        </div>
 </section>
 <div class="main-content">
        <section id="great-features" class="life-work-1 overview-block-pt iq-mt-50 software" style="margin-bottom:30px">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <img class="img-fluid" src="images/device/01.png" alt="drive05">
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <h2 class="iq-tw-6 iq-mt-70">About Our Company</h2>
                        <p class="iq-font-15">When you book meet and greet parking through ‘Park up fly’, the airport parking provider will meet you at the terminal or approved set down point, we  and take your car to their secure storage facility. Your car is delivered back to you upon your return, making your trip hassle free and your parking safe, secure and unbeatably convenient.</p>
                        <p class="iq-font-15">‘Park up fly’ aims to provide the best deals on both short stay airport parking as well as long term airport parking. Our affordable airport parking prices cannot be beaten on value and the level of service we provide. We offer valet parking at Heathrow and we will be expanding our services to other airports as well, giving you peace of mind while you are away on holiday or business. Get a quote for car parking today. Our cheap airport parking services always fit your budget.</p>
                        <p class="iq-font-15">We are the first and last point of contact between your journey, and it is crucial for our team to make this service a hassle free and an affordable service!</p>

                    </div>
                </div>
            </div>
        </section>

    </div>

        @endsection