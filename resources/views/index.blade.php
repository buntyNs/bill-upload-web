@extends ('layouts.master')




@section ('content')

    <!-- banner -->
    @include ('layouts.banner')
    <!-- banner end -->
  
<section id="how-it-works" class="overview-block-ptb it-works">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="heading-title">
                            <h3 class="title iq-tw-7">How it Works</h3>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley, </p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-4">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">1</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Best Price</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 r-mt-30">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">2</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Adjustable</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-4 r-mt-30">
                        <div class="iq-border-block text-left">
                            <div class="border-box">
                                <div class="step">3</div>
                                <h5 class="iq-tw-7 text-uppercase iq-mt-25 iq-mb-15">Years of Warranty</h5>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    <!-- <section id="feature" class="overview-block-ptb how-works grey-bg">
            <div class="container">
                <div class="row align-items-center justify-content-around">
                    <div class="col-md-12 col-lg-5">
                        <h3 class="iq-tw-2 iq-mb-25 r-mt-40">“You grow with it, and features you didn’t know would be useful are ready when you are.”</h3>
                        <h6>Steven Carse, co-founder, King of Pops</h6>
                    </div>
                    <div class="col-md-12 col-lg-5">
                        <img class="img-fluid r-mt-40 r-pb-40" src="images/home.png" alt="drive01">
                    </div>
                </div>
            </div>
    </section> -->

    <section id="software-features" class="life-work overview-block-ptb how-works">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <h2 class="iq-tw-6 iq-mb-25">Who is The Demo ?</h2>
                        <p class="iq-font-15">Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
                        <p class="iq-font-15 iq-mt-20">It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
                        <a href="/about-us" class="button iq-mt-10">Read More</a>
                    </div>
                    <div class="iq-software-demo-1">
                        <img class="img-fluid" src="images/15.jpg" alt="drive05">
                    </div>
                </div>
            </div>
        </section>




<!-- testimonial -->
<div id="testimonial" class="iq-testimonial2 overview-block-ptb iq-pb-120 text-center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="heading-title text-center">
                        <h2 class="title iq-tw-6">Client Feedback</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" >
                        @foreach ($userTestimonials as $userTestimonial)
                        <div class="item">
                            <div class="feedback">
                                <div class="iq-info text-left">
                                    <div class="iq-avtar iq-mr-20"> <img alt="#" class="img-fluid center-block" src="images/team/small/01.jpg"></div>
                                    <p>{!! $userTestimonial->body !!}</p>
                                </div>
                                <div class="iq-mt-30">
                                    <div class="avtar-name">
                                    <div class="iq-lead iq-mb-0 iq-tw-6 iq-font-blue">{{ ucwords($userTestimonial->customer) }}</div>
                                        <span>{{ ucwords($userTestimonial->city) }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>



            {{-- <div id="testimonial" class="iq-testimonial2 overview-block-ptb iq-pb-120 text-center">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="heading-title text-center">
                                    <h2 class="title iq-tw-6">Client Feedback</h2>
                                    <p>Lorem Ipsum is simply dummy text ever sincehar the 1500s, when an unknownshil printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" >
                                    @foreach ($userTestimonials as $userTestimonial)
                                    <div class="item">
                                        <div class="feedback">
                                            <div class="iq-info text-left">
                                                <div class="iq-avtar iq-mr-20"> <img alt="#" class="img-fluid center-block" src="images/team/small/01.jpg"></div>
                                                <p>{{ $userTestimonial->body }}</p>
                                            </div>
                                            <div class="iq-mt-30">
                                                <div class="avtar-name">
                                                    <div class="iq-lead iq-mb-0 iq-tw-6 iq-font-blue">{{ $userTestimonial->customer }}</div>
                                                    <span>{{ $userTestimonial->city }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

 --}}



        </div>
    </div>
<!-- testimonial END -->


    <!--================================
Pricing  -->
<section class="overview-block-pt iq-mt-20 grey-bg">
<div class="container">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="text-center text-width">
                <h3 class="iq-tw-2">You’ll be in good company</h3>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="iq-ptb-80">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="false" data-items="5" data-items-laptop="5" data-items-tab="4" data-items-mobile="3" data-items-mobile-sm="1" data-margin="30" >
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/01.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/02.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/03.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/04.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/05.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/06.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/07.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/08.png" alt="#"></div>
                    <div class="item"> <img class="img-fluid center-block" src="images/clients/09.png" alt="#"></div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--================================
Pricing  -->

<section id="get-in-tech" class="overview-block-ptb text-center yellow-bg">
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="white iq-font-black iq-mb-30">
                <h2 class="iq-tw-2 iq-mb-40">It’s easy to get started</h2>
                <h4 class="iq-tw-1">And it’s free. Two things everyone loves.</h4>
            </div>
            <a href="javascript:void(0)" class="button">Let's Started</a>
        </div>
    </div>
</div>
</section>
<hr>


@endsection
