@component('mail::message')

Thanks so much for registering with us {{ $user->first_name }}. We'll keep in touch.

@component('mail::button', ['url' => 'http://127.0.0.1:8000/login'])
Go to the website
@endcomponent

@component('mail::panel', ['url' => ''])
We will saves you money by negotiating lower rates with your current providers 
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
