@component('mail::message')

Hello again {{ ucwords($invoice->user) }}, we are happy to tell you that we 
have managed to negotiate the bills that you have send us.
<br>

You send us {{ $invoice->bill_type }} bill to the date of {{ $invoice->bill_date }} <br>
The original amount: {{ $invoice->original_amount }} <br>
The amount of the bill after our negotiation: {{ $invoice->new_amount }} <br>
The amount that we've reduced from your bill: {{ $invoice->negotiated_amount }} <br>
Our charge: {{ $invoice->our_charge }} <br>
New amount that you have to pay for your bill: {{ $invoice->paying_amount }} <br>

@component('mail::button', ['url' => 'https://127.0.0.1:8000/login'])
Pay the bill
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
