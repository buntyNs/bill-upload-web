@extends ('layouts.master-log')

    @section ('content')
			
	<!-- Wizard container -->
	<div class="wizard-container">

		<!-- You can switch ' data-color="orange" '  with one of the next bright colors: "blue", "green", "orange", "red" -->
		<div class="card wizard-card" data-color="orange">

			<form action="/login" method="post">
				@csrf
				<div class="wizard-header">
					<h3>
						<b>LOGIN TO</b> YOUR PROFILE <br>
					</h3>
				</div>

				<div class="">
					<div class="tab-pane">
						<div class="row">
							<h4 class="info-text">Please fill in the details</h4>
							@include('layouts.errors')
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="first-name">Email <small>*</small></label>
									<input name="email" type="text" id="email" class="form-control" required>
								</div>
							</div>
							<div class="col-sm-10 col-sm-offset-1">
								<div class="form-group">
									<label for="password">Password <small>*</small></label>
									<input name="password" type="password" id="password" class="form-control" required>
								</div>
							</div>
			
					</div>

					
					
				<div class="">
					<div style = "text-align: center;">
						<input type='submit' class='btn  btn-fill btn-warning btn-wd btn-sm' name='' value='Login' />
					</div>

					
					<div class="clearfix"></div>
				</div>

			</form>
		</div>

	</div> <!-- wizard container -->
    @endsection
