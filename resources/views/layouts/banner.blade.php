<section id="iq-home" class="iq-banner yellow-bg iq-bg-fixed" style = "background-color : #17a2b830">
    <div class="container">
        <div class="banner-text">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h1 class="iq-tw-3 iq-mt-30">Your business was born for this</h1>
                    <h4 class="iq-pt-20 iq-mb-30 iq-tw-2">Save up to $2500 on your monthly bills.</h4>
                    <a href="javascript:void(0)" class="button">Upload Now</a>
                </div>
                <div class="col-5">
                    <img class="img-fluid r-mt-40" src="/images/home.png" alt="logo">
                </div>
            </div>
        </div>
    </div>
</section>