<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="/plugins/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="/plugins/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>User Registration</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!--     Fonts and icons     -->
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.css" rel="stylesheet">

	<!-- CSS Files -->
    <link href="/plugins/assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/plugins/assets/css/gsdk-bootstrap-wizard.css" rel="stylesheet" />

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="/plugins/assets/css/demo.css" rel="stylesheet" />
</head>

<body>
<div class="image-container set-full-height">
    <!--   Creative Tim Branding   -->
    <a href="/">
         <div class="logo-container">
            <div class="logo">
                <img src="/plugins/assets/img/new_logo.png">
            </div>
            <div class="brand">
                Company Name
            </div>
        </div>
	</a>

	<!--   Big container   -->
    <div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">

			@yield ('content')
	
			</div>
		</div><!-- end row -->
	</div><!--  big container -->

@include ('layouts.footer-log')
