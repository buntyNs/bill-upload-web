<header id="main-header" class="dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="/">
                        <img class="img-fluid" id="logo_img" src="/images/logo.png" alt="#">
                    </a>
                    <button class="navbar-toggler navbar-nav mr-auto w-100 justify-content-end" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="ion-navicon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto w-100 justify-content-end">

                        <li class="nav-item">
                            <a class="nav-link" href="/">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/about-us">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/faq">FAQ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#iq-contact">Contact</a>
                            </li>

                            @if (!Auth::check())
                            <li class="nav-item">
                                <a class="nav-link" href="/register" style = "color:#0eb0dcdb">Register</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/login" style = "color:#0eb0dcdb">Login</a>
                            </li>
                            @endif

                            @if (Auth::check())
                            <li class="nav-item">
                            {{-- <a href="/dashboard" class="nav-link">{{ ucfirst(Auth::user()->first_name) }}</a> --}}
                            <a @if (Auth::user()->role == 'admin')
                                href="/admin/dashboard"
                            @else
                                href="/dashboard"
                            @endif class="nav-link"><img src="/images/avatar.png" alt="Avatar" style="vertical-align:middle; width:30px; height:30px; border-radius:50%;"> {{ ucfirst(Auth::user()->first_name) }}</a>
                            </li>
                            @endif



                        </ul>




                        

                    </div>
                </nav>
            </div>
        </div>
    </div>
</header>
