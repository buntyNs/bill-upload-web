<!doctype html>
<html lang="en">
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="/images/favicon.ico" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;Raleway:300,400,500,600,700,800,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- owl-carousel -->
    <link rel="stylesheet" href="/css/owl-carousel/owl.carousel.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.css" />
    <!-- Magnific Popup -->
    <link rel="stylesheet" href="/css/magnific-popup/magnific-popup.css" />
    <!-- media element player -->
    <link href="/css/mediaelementplayer.min.css" rel="stylesheet" type="text/css" />
    <!-- Animate -->
    <link rel="stylesheet" href="/css/animate.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="/css/ionicons.min.css">
    <!-- Style -->
    <link rel="stylesheet" href="/css/style.css">
    <!-- Responsive -->
    <link rel="stylesheet" href="/css/responsive.css">
    <!-- custom style -->
    <link rel="stylesheet" href="/css/custom.css" />

    <link rel="stylesheet" href="/css/profile.css" />

    

    </head>
    <body data-spy="scroll" data-offset="80">
    
    <!-- loading -->
    @include('layouts.loading')
    <!-- loading end -->

    <!-- navbar -->
    @include ('layouts.top-navbar')
    <!-- navbar end -->

    <!-- main content -->
    <div class="main-content">

        {{-- @if ($flash = session('message'))
            <nav id="flash-message" class="alert alert-success" role="alert">
            {{ $flash }}
            </nav>
        @endif --}}


        @yield ('content')

    </div>
    <!-- main content end -->

    <!-- dashboard footer -->
    @include('layouts.footer')
    <!-- dashboard footer -->
