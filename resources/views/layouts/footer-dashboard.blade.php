<footer class="iq-footer overview-block-pt iq-pb-20" id="iq-contact">

        <div class="container">
            <div class="text-box text-center iq-font-black">
               <div class="iq-copyright iq-mt-30 iq-font-black">
                  Copyright @ 2019 All Rights Reserved
               </div>
            </div>
        </div>
     </footer>
     <!--=================================
        Footer -->

       <!-- back-to-top -->
       <div id="back-to-top">
           <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
       </div>
       <!-- back-to-top End -->
       <!-- Optional JavaScript -->
       <!-- jQuery first, then Popper.js, then Bootstrap JS -->
       <script>
       var myIndex = 0;
       carousel();
       function carousel() {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
        }
        myIndex++;
        if (myIndex > x.length) {myIndex = 1}
        x[myIndex-1].style.display = "block";
        setTimeout(carousel, 2000); // Change image every 2 seconds
        }
       </script>
       <script src="js/jquery.min.js"></script>
       <script src="js/popper.min.js"></script>
       <script src="js/bootstrap.min.js"></script>
       <!-- Main js -->
       <script src="js/main.js"></script>
       <!-- Google captcha code Js -->
       <script src='https://www.google.com/recaptcha/api.js'></script>
       <!-- Custom -->
       <script src="js/custom.js"></script>
       <!-- Style Customizer -->
       <script src="js/style-customizer.js"></script>
   </body>
</html>
