
  <footer id="iq-contact" class="iq-footer3 iq-ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 iq-mtb-20">
                    <div class="contact-bg">
                        <h5 class="iq-tw-6 iq-font-white iq-mb-20">Address</h5>
                        <ul class="iq-contact">
                            <li>
                                <i class="ion-ios-location-outline"></i>
                                <p>1234 North Luke Lane, South Bend, IN 360001</p>
                            </li>
                            <li>
                                <i class="ion-ios-telephone-outline"></i>
                                <p>+0123 456 789</p>
                            </li>
                            <li>
                                <i class="ion-ios-email-outline"></i>
                                <p>mail@iqonictheme.com</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 iq-mtb-20">
                    <h5 class="iq-tw-6 iq-font-white iq-mb-20">Office Days</h5>
                    <ul class="addresss-info">
                        <li>
                            <p>MON - FRI: 09AM - 05PM</p>
                        </li>
                        <li>
                            <p>SAT: 09AM - 01PM</p>
                        </li>
                        <li>
                            <p>SUN: Enjoy Day</p>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 iq-mtb-20">
                    <h5 class="iq-tw-6 iq-font-white iq-mb-20">Menu</h5>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="menu">
                                <li><a href="javascript:void(0)">Home</a></li>
                                <li><a href="javascript:void(0)">About Us</a></li>
                                <li><a href="javascript:void(0)">Features</a></li>
                                <li><a href="javascript:void(0)">Services</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <ul class="menu">
                                <li><a href="javascript:void(0)">Faqs</a></li>
                                <li><a href="javascript:void(0)">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 iq-mtb-20">
                    <h5 class="iq-tw-6 iq-font-white iq-mb-20">Newsletter</h5>
                    <input type="email" class="form-control" placeholder="name@example.com">
                    <a class="button iq-mt-20" href="javascript:void(0)">Subscribe Now</a>
                </div>
            </div>
            <hr>
            <div class="row iq-mt-20">
                <div class="col-lg-6 col-md-5 col-sm-12">
                    <ul class="link">
                        <li class="d-inline-block iq-mr-20"><a href="javascript:void(0)">Term and Condition</a></li>
                        <li class="d-inline-block"><a href="javascript:void(0)"> Privacy Policy</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-12">
                    <div class="iq-copyright">
                        © Copyright 2018 Sofbox Developed by <a href="">Iqonic Themes</a>.
                    </div>
                </div>
            </div>
        </div>
    </footer>
        
     <!--=================================
        Footer -->

       <!-- back-to-top -->
       <div id="back-to-top">
           <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
       </div>
       <!-- back-to-top End -->
       <!-- Optional JavaScript -->
       <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <!-- Main js -->
    <script src="/js/main.js"></script>
    <!-- Custom -->
    <script src="/js/custom.js"></script>
    <script>
        window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, 
            function() {
                $(this).remove(); 
            });
        }, 3000);
    </script>
   </body>
</html>
