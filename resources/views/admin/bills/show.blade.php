@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/bills">Bills</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <h3 class="card-inside-title">Bill No</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $bill->bill_no }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">User</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Bill type</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($bill->bill_type) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Service provider</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($bill->service_provider) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Duration of the service</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($bill->duration_of_service) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Bill</h3>
                        <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <a href="/storage/{{ $bill->image_url }}" data-sub-html="{{ ucfirst($bill->bill_type) }} bill">
                                    <img class="img-responsive thumbnail" src="/storage/{{ $bill->image_url }}">
                                </a>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Date of the bill</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $bill->date }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if (isset($bill->pass_code))
                        <h3 class="card-inside-title">Pass code</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $bill->pass_code }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                
                        @if (isset($bill->comment))
                        <h3 class="card-inside-title">Comment</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" class="form-control no-resize" disabled>{{ ucfirst($bill->comment) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif

                        <h3 class="card-inside-title">Status</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($bill->status) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <small>Uploaded on {{ $bill->created_at->toFormattedDateString() }}</small>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
    <script src="/admin/js/admin.js"></script>
@endsection
