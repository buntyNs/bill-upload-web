@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- Light Gallery Plugin Css -->
    <link href="/admin/plugins/light-gallery/css/lightgallery.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-invoices">Manage Invoices</a></li>
                    <li class="active">Create</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="/admin/dashboard/manage-invoices" method="POST">
                            @csrf
                            <input type="hidden" name="user-id" value="{{ $user->id }}">
                            <input type="hidden" name="id" value="{{ $bill->id }}">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="bill-id">Bill No</label>
                                    <input type="text" id="bill-id" class="form-control" name="bill-id" value="{{ $bill->bill_no }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="bill-type">Bill Type</label>
                                    <input type="text" id="bill-type" class="form-control" name="bill-type" value="{{ ucfirst($bill->bill_type) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="service-provider">Service Provider</label>
                                    <input type="text" id="service-provider" class="form-control" name="service-provider" value="{{ ucfirst($bill->service_provider) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="user">Associated User</label>
                                    <input type="text" id="user" class="form-control" name="user" value="{{ ucfirst($user->first_name) }} {{ ucfirst($user->last_name) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="duration">Duration</label>
                                    <input type="text" id="duration" class="form-control" name="duration" value="{{ ucfirst($bill->duration_of_service) }}" readonly>
                                </div>
                            </div>
                            @if (isset($bill->pass_code))
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="pass-code">Pass Code</label>
                                    <input type="text" id="pass-code" class="form-control" name="pass-code" value="{{ $bill->pass_code }}" readonly>
                                </div>
                            </div>
                            @endif
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="date">Date of the bill</label>
                                    <input type="text" id="date" class="form-control" name="date" value="{{ $bill->date }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <label for="date">Bill</label>
                                <div id="aniimated-thumbnials" class="list-unstyled row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <a href="/storage/{{ $bill->image_url }}" data-sub-html="{{ ucfirst($bill->bill_type) }} bill">
                                            <img class="img-responsive thumbnail" src="/storage/{{ $bill->image_url }}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @if (isset($bill->pass_code))
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="comment">Bill Comment</label>
                                    <textarea id="comment" class="form-control no-resize" name="comment" readonly>{{ $bill->comment }}</textarea>
                                </div>
                            </div>
                            @endif
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="original-amount">Original amount of the bill</label>
                                    <input type="text" id="original-amount" class="form-control" min="0" max="10000" name="original-amount" required>
                                    <div class="help-info">Min. Value: 0, Max. Value: 10000</div>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="new-amount">Amount of the bill after negotiation</label>
                                    <input type="text" id="new-amount" class="form-control" min="0" max="10000" name="new-amount" required>
                                    <div class="help-info">Min. Value: 0, Max. Value: 10000</div>
                                </div>
                            </div>

                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>

                        @include('layouts.errors')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Light Gallery Plugin Js -->
    <script src="/admin/plugins/light-gallery/js/lightgallery-all.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/medias/image-gallery.js"></script>
@endsection
