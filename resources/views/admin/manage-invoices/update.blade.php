@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-invoices">Manage Invoices</a></li>
                    <li class="active">Update</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="/admin/dashboard/manage-invoices/update/{{ $invoice->id }}" method="POST">
                            @csrf

                            <input type="hidden" id="invoice-id" class="form-control" name="invoice-id" value="{{ $invoice->id }}" readonly>

                            <div class="form-group form-float">
                                    <div class="form-line">
                                        <label for="invoice-no">Invoice No</label>
                                        <input type="text" id="invoice-no" class="form-control" name="invoice-no" value="{{ $invoice->invoice_no }}" readonly>
                                    </div>
                                </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="bill-id">Bill No</label>
                                    <input type="text" id="bill-id" class="form-control" name="bill-id" value="{{ $invoice->bill_id }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="user">Associated user</label>
                                    <input type="text" id="user" class="form-control" name="user" value="{{ ucfirst($invoice->user) }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="date">Date of the bill</label>
                                    <input type="text" id="date" class="form-control" name="date" value="{{ $invoice->bill_date }}" readonly>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="original-amount">Original amount of the bill</label>
                                    <input type="text" id="original-amount" class="form-control" min="0" max="10000" name="original-amount" value="{{ $invoice->original_amount }}">
                                    <div class="help-info">Min. Value: 0, Max. Value: 10000</div>
                                </div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="new-amount">Amount of the bill after negotiation</label>
                                    <input type="text" id="new-amount" class="form-control" min="0" max="10000" name="new-amount" value="{{ $invoice->new_amount }}">
                                    <div class="help-info">Min. Value: 0, Max. Value: 10000</div>
                                </div>
                            </div>

                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>

                        @include('layouts.errors')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
@endsection
