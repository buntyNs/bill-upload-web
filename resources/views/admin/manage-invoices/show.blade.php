@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-invoices">Manage Invoices</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <h3 class="card-inside-title">Invoice No</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->invoice_no }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Bill No</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->bill_id }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">User</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->user }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>


                        <h3 class="card-inside-title">Date</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->bill_date }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Bill uploaded date</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->bill_created_date }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        
                        <h3 class="card-inside-title">Original bill amount</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->original_amount }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">New bill amount</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->new_amount }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Negotiated bill amount</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->negotiated_amount }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Charged amount for the bill</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->our_charge }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Customer paying amount for the bill</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->paying_amount }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <h3 class="card-inside-title">Status</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $invoice->status }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <small>Created on {{ $invoice->created_at->toFormattedDateString() }}</small>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
@endsection
