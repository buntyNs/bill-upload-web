@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/manage-users">Manage Users</a></li>
                    <li class="active">View</li>
                </ol>

                <div class="card"> 
                    <div class="body">
                        <h3 class="card-inside-title">ID</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->id }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">First name</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($user->first_name) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Last name</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($user->last_name) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Additional bill holder</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ ucfirst($user->additional_bill_holder) }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Email</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->email }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Phone number</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->phone_number }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Last 4 digits of social security number</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->social_security }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Date of birth</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->dob }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Address</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" class="form-control no-resize" disabled>{{ ucfirst($user->address) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3 class="card-inside-title">Promo code</h3>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line disabled">
                                        <input type="text" class="form-control" value="{{ $user->promo_code }}" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <small>Registered on {{ $user->created_at->toFormattedDateString() }}</small>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
@endsection
