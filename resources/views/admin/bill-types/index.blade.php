@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <!-- Sweet Alert Css -->
    <link href="/admin/plugins/sweetalert/sweetalert.css" rel="stylesheet" />
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li class="active">Bill Types</li>
                </ol>

                <div class="card">
                    <div class="header">
                        <h2>
                            ALL BILL TYPES
                        </h2>
                        <ul class="header-dropdown m-r--5">

                            <div class="button-demo js-modal-buttons">
                                <button type="button" data-color="indigo" class="btn bg-indigo waves-effect" data-toggle="modal" data-target="#mdModal">Create new</button>
                            </div>

                            <div class="modal fade" id="mdModal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="defaultModalLabel">Create New Bill Type</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form id="form_advanced_validation" action="/admin/dashboard/bill-types" method="POST">
                                                @csrf
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="bill-type" maxlength="20" minlength="3" required>
                                                        <label class="form-label">Please enter a name for the bill type</label>
                                                    </div>
                                                    <div class="help-info">Min. 3, Max. 20 characters</div>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                                            </form>
                                            <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </ul>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Bill Type</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </thead>
                            @if (count($billTypes) >= 15)
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Bill Type</th>
                                    <th>&nbsp;</th>
                                </tr>
                            </tfoot>
                            @endif
                            <tbody>
                                @foreach ($billTypes as $billType)
                                <tr>
                                    <td>{{ $billType->id }}</td>
                                    <td>{{ ucfirst($billType->name) }}</td>
                                    <td>
                                        <a href="#" delete={{ $billType->id }}  class="delete">
                                            <i class="material-icons">delete</i>
                                        </a>
                                        <a href="#" data-toggle="modal" data-target="#{{ $billType->name }}">
                                            <i class="material-icons">mode_edit</i>
                                        </a>
                                        <div class="modal fade" id="{{ $billType->name }}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title" id="defaultModalLabel">Edit Bill Type</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form id="form_advanced_validation" action="/admin/dashboard/bill-types/update/{{ $billType->id }}" method="POST">
                                                            @csrf
                                                            <div class="form-group form-float">
                                                                <div class="form-line">
                                                                <input type="text" class="form-control" name="bill-type" maxlength="20" minlength="3" value="{{ $billType->name }}" required>
                                                                    <label class="form-label"></label>
                                                                </div>
                                                                <div class="help-info">Min. 3, Max. 20 characters</div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-link waves-effect">SAVE CHANGES</button>
                                                        </form>
                                                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if ($flash = session('message'))  
            <div class="alert bg-green alert-dismissible text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $flash }}
            </div>
        @endif
        @if ($flash = session('message-error'))  
            <div class="alert bg-red alert-dismissible text-center" role="alert" style="position:absolute; bottom:5px; right:30px; z-index:20">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ $flash }}
            </div>
        @endif
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.min.js"></script>
    <script src="/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    {{-- <script src="/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script> --}}
    {{-- <script src="/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script> --}}
    <script src="/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/tables/jquery-datatable.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/ui/dialogs.js"></script>

    <script>
        $(".delete").on("click", function() {
            showCancelMessage(this.getAttribute("delete"), this);
        });

        function showCancelMessage(id, dom) {
            swal({
                title: "Are you sure want to delete this?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "GET",
                        url: "/admin/dashboard/bill-types/delete/" + id,
                        success: function(msg){
                            swal("Deleted!", "Successfully deleted", "success");
                            $(dom).closest("tr").remove();
                        }
                    });
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }

        window.setTimeout(function() {
            $(".alert").fadeTo(500, 0).slideUp(500, 
        function(){
            $(this).remove(); 
            });
        }, 3000);
    </script>
@endsection
