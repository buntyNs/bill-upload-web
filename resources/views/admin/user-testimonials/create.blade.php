@extends('layouts-admin.master')

@section('page-css')
    <!-- Waves Effect Css -->
    <link href="/admin/plugins/node-waves/waves.min.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="/admin/plugins/animate-css/animate.min.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <!-- braedcrumb menu -->
                <ol class="breadcrumb breadcrumb-col-cyan">
                    <li><a href="/admin/dashboard">Dashboard</a></li>
                    <li><a href="/admin/dashboard/user-testimonials">User Testimonials</a></li>
                    <li class="active">Create</li>
                </ol>

                <div class="card">
                    <div class="body">
                        <form id="form_advanced_validation" action="/admin/dashboard/user-testimonials" method="POST">
                            @csrf
                            @include('layouts.errors')
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="topic">User testimonial topic</label>
                                    <input type="text" id="topic" class="form-control" name="topic" maxlength="50" minlength="10" required>
                                </div>
                                <div class="help-info">Min. 10, Max. 50 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="customer">User testimonial owner</label>
                                    <input type="text" id="customer" class="form-control" name="customer" maxlength="20" minlength="5" required>
                                </div>
                                <div class="help-info">Min. 5, Max. 20 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="city">User testimonial owner's city</label>
                                    <input type="text" id="city" class="form-control" name="city" maxlength="30" minlength="2" required>
                                </div>
                                <div class="help-info">Min. 2, Max. 20 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <label for="ckeditor">User testimonial body</label><br><br>
                                    <textarea id="ckeditor" class="form-control no-resize" maxlength="500" minlength="10" name="body" required></textarea>
                                </div>
                                <div class="help-info">Min. 10, Max. 500 characters</div>
                            </div>
                            <div class="form-group form-float">
                                <label for="status">Status</label>
                                <div class="demo-radio-button">
                                    <input name="status" type="radio" id="published" value="published" class="with-gap radio-col-indigo" />
                                    <label for="published">PUBLISH</label>
                                    <input name="status" type="radio" id="unpublished" value="unpublished" class="with-gap radio-col-indigo" checked />
                                    <label for="unpublished">UNPUBLISH</label>
                                </div>
                            </div>
                            <br>
                            <button class="btn btn-primary waves-effect" type="submit">SUBMIT</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('page-scripts')
    <!-- Select Plugin Js -->
    <script src="/admin/plugins/bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="/admin/plugins/node-waves/waves.min.js"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="/admin/plugins/jquery-steps/jquery.steps.min.js"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="/admin/plugins/sweetalert/sweetalert.min.js"></script>

    <!-- Ckeditor -->
    <script src="/admin/plugins/ckeditor/ckeditor.js"></script>

    <!-- Custom Js -->
    <script src="/admin/js/admin.js"></script>
    <script src="/admin/js/pages/forms/form-validation.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
@endsection
